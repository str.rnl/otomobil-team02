
import * as React from "react";
import { CardContent, Typography, Card,} from "@mui/material";


export default function Cardss(item){
<Card sx={{ maxWidth: 345 }}>
    <CardContent>
        <Typography gutterBottom variant="h5" component="div">
            item.value
        </Typography>
        <Typography variant="body2" color="text.secondary">
            item.desc
        </Typography>
    </CardContent>
</Card>
}
