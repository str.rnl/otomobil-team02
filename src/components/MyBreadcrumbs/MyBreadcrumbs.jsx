import React from "react";
import { Breadcrumbs, Typography } from "@mui/material";
import { Link, useLocation, matchPath } from "react-router-dom";
import { routes } from "../../routes";

const MyBreadcrumbs = () => {
  const { pathname } = useLocation();
  //console.log(routes[6]);

  const paths = pathname.split("/").filter((item) => item !== "");

  const crumbs = [{ ...routes.find((obj) => obj.path === "/"), to: "/" }];
  let url = "";
  paths.forEach((item) => {
    url += `/${item}`;

    routes.forEach((route) => {
      const matched = matchPath(route.path, url);
      if (matched) {
        crumbs.push({ ...route, to: matched.pathname });
      }
    });
  });
  return (
    <Breadcrumbs separator=">" sx={{ marginTop: "36px" }}>
      {crumbs.map((crumb, index) => {
        if (crumbs.length === index + 1) {
          return (
            <Typography key={crumb.to} sx={{ fontWeight: 600 }} color="primary">
              {crumb.name}
            </Typography>
          );
        }

        return (
          <Link
            style={{
              textDecoration: "none",
              fontWeight: 600,
              color: "rgba(0,0,0,0.6)",
            }}
            key={crumb.to}
            to={crumb.to}
          >
            {crumb.name}
          </Link>
        );
      })}
    </Breadcrumbs>
  );
};

export default MyBreadcrumbs;
