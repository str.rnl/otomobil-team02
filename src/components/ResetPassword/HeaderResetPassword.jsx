import { Box, Typography } from "@mui/material";
import React from "react";

export default function HeaderResetPassword({ type }) {
  const checkType = () => {
    if (type === "confirm_email") {
      return {
        title: "Reset Password",
        subtitle: "Send OTP code to your email address",
      };
    }

    if (type === "create") {
      return { title: "Create Password", subtitle: "" };
    }
  };
  const headerData = checkType();

  return (
    <Box sx={{ marginTop: "96px", marginBottom: "60px" }}>
      <Typography variant="h4" sx={{ marginBottom: "16px", color: "#333" }}>
        {headerData?.title}
      </Typography>
      {headerData?.subtitle ? (
        <Typography variant="h6" sx={{ color: "#4F4F4F" }}>
          {headerData?.subtitle}
        </Typography>
      ) : (
        ""
      )}
    </Box>
  );
}
