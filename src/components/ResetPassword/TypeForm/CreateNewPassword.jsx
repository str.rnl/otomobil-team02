import { Box, Button, TextField } from "@mui/material";
import React, { useState } from "react";
import axios from "../../../utils/axiosConfig";
import { useNavigate, useSearchParams } from "react-router-dom";

export default function CreateNewPassword() {
  const [params] = useSearchParams();
  const getQ = params.get("q");
  const users_id = decodeURIComponent(getQ);
  const navigate = useNavigate();

  const [formData, setFormData] = useState({
    password: "",
    confirm_password: "",
  });

  const [validationErrors, setValidationErrors] = useState({
    password: "",
    confirm_password: "",
  });

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });

    //Validation Input
    validateInput(name, value);
  };

  const validateInput = (name, value) => {
    const errors = {};
    if (value.length < 5) {
      errors[name] =
        name === "password"
          ? "The field new password must minimum length of 5."
          : "The field confirm password must minimum length of 5.";
    } else if (name === "confirm_password" && value !== formData.password) {
      errors[name] =
        "The field confirm password not same with field new password";
    } else {
      errors[name] = "";
    }

    setValidationErrors({ ...validationErrors, ...errors });
  };

  function areAllValuesEmptyString(obj) {
    return Object.values(obj).every(
      (value) => typeof value === "string" && value.trim() === ""
    );
  }

  const handleSubmitForm = (e) => {
    e.preventDefault();

    if (
      areAllValuesEmptyString(validationErrors) &&
      !areAllValuesEmptyString(formData)
    ) {
      axios
        .put("/api/new_password", {
          users_password: formData.password,
          users_id: users_id,
        })
        .then((response) => {
          if (response.status === 200) {
            navigate("/login");
          }
        })
        .catch((error) => {
          console.error("Something went wrong :", error);
        });
    }
  };

  return (
    <form onSubmit={handleSubmitForm}>
      <Box sx={{ marginTop: "68px" }}>
        <Box sx={{ display: "flex", flexDirection: "column", gap: "32px" }}>
          <TextField
            autoFocus
            type="password"
            fullWidth
            label="New Password"
            variant="outlined"
            size="small"
            name="password"
            value={formData.password}
            onChange={handleChangeInput}
            error={validationErrors.password ? true : false}
            helperText={
              validationErrors.password === "" ? "" : validationErrors.password
            }
          />
          <TextField
            type="password"
            fullWidth
            label="Confirm New Password"
            variant="outlined"
            size="small"
            name="confirm_password"
            value={formData.confirm_password}
            onChange={handleChangeInput}
            error={validationErrors.confirm_password ? true : false}
            helperText={
              validationErrors.confirm_password === ""
                ? ""
                : validationErrors.confirm_password
            }
          />
        </Box>
        <Box
          sx={{
            display: "flex",
            justifyContent: "end",
            gap: "34px",
            marginTop: "50px",
          }}
        >
          <Button
            type="button"
            variant="outlined"
            color="primary"
            sx={{ width: "120px", borderRadius: "8px" }}
          >
            Cancel
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            sx={{ width: "120px", borderRadius: "8px" }}
          >
            Submit
          </Button>
        </Box>
      </Box>
    </form>
  );
}
