import { Box, Button, TextField } from "@mui/material";
import React, { useState } from "react";
import { useCheckValue } from "../../../hooks/useCheckValue";
import MyAlert from "../../MyAlert/MyAlert";
import axios from "../../../utils/axiosConfig";
import { useNavigate } from "react-router-dom";
import { LoadingButton } from "@mui/lab";

export default function ConfirmEmail() {
  const [formData, setFormData] = useState({
    email: "",
  });
  const [validationErrors, setValidationErrors] = useState({
    email: "",
  });

  const [isAlert, setIsAlert] = useState(false);
  const [alertMsg, setAlertMsg] = useState({
    type: "",
    msg: "",
  });

  const navigate = useNavigate();

  const [isSubmit, setIsSubmit] = useState(false);

  const checkFormData = useCheckValue(formData);
  const checkValidationErrors = useCheckValue(validationErrors);

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });

    //validation input
    validateInput(name, value);
  };

  const validateInput = (name, value) => {
    const errors = {};
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/; //email

    if (value.length === 0) {
      errors[name] = `The field ${name} is required`;
    } else if (name === "email" && !emailRegex.test(value)) {
      errors[name] = "The email address is not valid";
    } else {
      errors[name] = "";
    }

    setValidationErrors({ ...validationErrors, ...errors });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (checkValidationErrors && !checkFormData) {
      try {
        setIsSubmit(true);
        const { data, status } = await axios.post("/api/validate_email", {
          email: formData.email.toLowerCase(),
        });

        if (status === 200) {
          setIsSubmit(false);
          navigate(
            "/reset-password/success?q=" + encodeURIComponent(data.user_id)
          );
        }
      } catch (error) {
        setIsAlert(true);
        setAlertMsg({
          ...alertMsg,
          type: "Error",
          msg: error.response.data.message,
        });
        setIsSubmit(false);
      }
    }
  };

  return (
    <>
      <MyAlert isAlert={isAlert} setIsAlert={setIsAlert} alertMsg={alertMsg} />
      <form onSubmit={handleSubmit}>
        <Box sx={{ marginTop: "0" }}>
          <TextField
            autoFocus
            type="email"
            fullWidth
            label="Email"
            variant="outlined"
            size="small"
            name="email"
            value={formData.email}
            onChange={handleChangeInput}
            onBlur={handleChangeInput}
            error={validationErrors.email ? true : false}
            helperText={
              validationErrors.email === "" ? "" : validationErrors.email
            }
            disabled={isSubmit}
            required
          />
          <Box
            sx={{
              display: "flex",
              justifyContent: "end",
              gap: "34px",
              marginTop: "50px",
            }}
          >
            <Button
              type="button"
              variant="outlined"
              color="primary"
              onClick={() => navigate("/login")}
              sx={{ width: "120px", borderRadius: "8px" }}
            >
              Cancel
            </Button>
            <LoadingButton
              type="submit"
              variant="contained"
              color="primary"
              loading={isSubmit}
              sx={{ width: "120px", borderRadius: "8px" }}
            >
              Confirm
            </LoadingButton>
          </Box>
        </Box>
      </form>
    </>
  );
}
