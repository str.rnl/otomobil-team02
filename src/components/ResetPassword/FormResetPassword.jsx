import React from "react";
import ConfirmEmail from "./TypeForm/ConfirmEmail";
import CreateNewPassword from "./TypeForm/CreateNewPassword";

export default function FormResetPassword({ type }) {
  const TypeForm = () => {
    if (type === "confirm_email") {
      return <ConfirmEmail />;
    }

    if (type === "create") {
      return <CreateNewPassword />;
    }
  };

  return <TypeForm />;
}
