import React, { useState } from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Logo from "../../assets/otomobil-logo.svg";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { useAuth } from "../../contexts/AuthContext";
import GuestMenu from "./Menu/GuestMenu";
import AuthMenu from "./Menu/AuthMenu";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Divider,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemText,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";

const drawerWidth = 300;
const navItems = [
  [
    { title: "Sign Up", to: "/register" },
    { title: "Login", to: "/login" },
  ],
  [
    {
      title: "My Checkout",
      to: "/checkout",
    },
    { title: "My Class", to: "/my-class" },
    { title: "Invoice", to: "/invoice" },
    { title: "Profile", to: "/admin/users" },
    { title: "Logout", to: "" },
  ],
];

export default function Navbar() {
  const { pathname } = useLocation();
  const { isAuth, setIsAuth, handleLogout } = useAuth();
  const [mobileOpen, setMobileOpen] = useState(false);
  const navigate = useNavigate();

  const filtered_pages = ["/admin/users", "/admin/payments", "/admin/invoices"];

  const handleTo = (path) => {
    if (path) {
      navigate(path);
    } else {
      handleClickOpen();
      //handleLogout();
    }
  };

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };

  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = (isLogout) => {
    if (isLogout) {
      handleLogout();
    }
    setOpen(false);
  };

  if (filtered_pages.includes(pathname)) {
    return;
  }

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: "center" }}>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          my: 2,
          justifyContent: "center",
        }}
      >
        <Link
          to="/"
          style={{
            textDecoration: "none",
            color: "black",
            display: "flex",
            alignItems: "center",
          }}
        >
          <img src={Logo} alt="otomobil-logo"></img>
          <Typography variant="h6">Otomobil</Typography>
        </Link>
      </Box>
      <Divider />
      <List>
        {navItems[isAuth ? 1 : 0].map((item, index) => (
          <ListItem
            key={index}
            disablePadding
            onClick={() => handleTo(item.to)}
          >
            <ListItemButton sx={{ textAlign: "center" }}>
              <ListItemText primary={item.title} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  const container =
    typeof window !== "undefined" ? window.document.body : undefined;

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar
        component="nav"
        position="static"
        sx={{
          background: "transparent",
          paddingX: { xl: "60px", xs: "0" },
          color: "black",
          boxShadow: "none",
        }}
      >
        <Toolbar>
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, paddingX: "0" }}
          >
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <Link
                to="/"
                style={{
                  textDecoration: "none",
                  color: "black",
                  display: "flex",
                  alignItems: "center",
                }}
              >
                <img src={Logo} alt="otomobil-logo"></img>
                <Typography variant="h6">Otomobil</Typography>
              </Link>
            </Box>
          </Typography>
          <Box sx={{ display: { xs: "none", md: "flex" }, gap: "16px" }}>
            {!isAuth ? (
              <GuestMenu setIsAuth={setIsAuth} />
            ) : (
              <AuthMenu setIsAuth={setIsAuth} />
            )}
          </Box>
          <Box sx={{ display: { xs: "flex", md: "none" } }}>
            <IconButton
              color="inherit"
              edge="start"
              onClick={handleDrawerToggle}
            >
              <MenuIcon />
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
      <Box component="nav">
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          anchor="right"
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: "block", md: "none" },
            "& .MuiDrawer-paper": {
              boxSizing: "border-box",
              width: drawerWidth,
            },
          }}
        >
          {drawer}
        </Drawer>
      </Box>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        maxWidth="sm"
        fullWidth={true}
      >
        <DialogTitle id="alert-dialog-title">
          {"Are you sure to Logout?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            You can logout by click "yes" button below.
          </DialogContentText>
        </DialogContent>
        <DialogActions sx={{ padding: "16px" }}>
          <Button onClick={() => handleClose(false)}>Cancel</Button>
          <Button
            variant="contained"
            onClick={() => handleClose(true)}
            autoFocus
          >
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
  );
}
