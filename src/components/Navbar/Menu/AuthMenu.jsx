import Button from "@mui/material/Button";
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Tooltip,
} from "@mui/material";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import LogoutIcon from "@mui/icons-material/Logout";
import PersonIcon from "@mui/icons-material/Person";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import { useAuth } from "../../../contexts/AuthContext";
import { useState } from "react";

const AuthMenu = ({ setIsAuth }) => {
  const { handleLogout } = useAuth();
  const navigate = useNavigate();

  const handleToAdmin = () => {
    const { users_role } = JSON.parse(localStorage.getItem("user"));
    if (users_role === "admin") {
      navigate("/admin/users");
    } else {
      navigate(localStorage.getItem("last_location"));
    }
  };
  const [open, setOpen] = useState(false);

  const handleToLogOut = () => {
    setOpen(true);
  };

  const handleClose = (isLogout) => {
    if (isLogout) {
      handleLogout();
      setOpen(false);
    } else {
      setOpen(false);
    }
  };

  return (
    <>
      <Tooltip title="Open Cart" arrow>
        <IconButton onClick={() => navigate("/checkout")} color="primary">
          <ShoppingCartIcon />
        </IconButton>
      </Tooltip>
      <Button
        color="primary"
        sx={{
          borderRadius: "8px",
          paddingX: "20px",
          paddingY: "10px",
          ":hover": {
            background: "transparent",
          },
        }}
        onClick={() => navigate("/myclass")}
      >
        My Class
      </Button>
      <Button
        color="primary"
        sx={{
          borderRadius: "8px",
          paddingX: "20px",
          paddingY: "10px",
          ":hover": {
            background: "transparent",
          },
        }}
        onClick={() => navigate("/invoice")}
      >
        Invoice
      </Button>
      <Button
        color="primary"
        sx={{
          borderRadius: "8px",
          paddingX: "20px",
          paddingY: "10px",
          ":hover": {
            background: "transparent",
          },
        }}
      >
        |
      </Button>
      <Tooltip title="Profile" arrow>
        <IconButton color="primary" onClick={handleToAdmin}>
          <PersonIcon />
        </IconButton>
      </Tooltip>
      <Tooltip title="Logout" arrow>
        <IconButton color="primary" onClick={handleToLogOut}>
          <LogoutIcon />
        </IconButton>
      </Tooltip>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        maxWidth="sm"
        fullWidth={true}
      >
        <DialogTitle id="alert-dialog-title">
          {"Are you sure to Logout?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            You can logout by click "yes" button below.
          </DialogContentText>
        </DialogContent>
        <DialogActions sx={{ padding: "16px" }}>
          <Button onClick={() => handleClose(false)}>Cancel</Button>
          <Button
            variant="contained"
            onClick={() => handleClose(true)}
            autoFocus
          >
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

AuthMenu.propTypes = {
  setIsAuth: PropTypes.func,
};

export default AuthMenu;
