import React from "react";
import Button from "@mui/material/Button";
import PropTypes from "prop-types";
import { useNavigate } from "react-router";

function GuestMenu({ setIsAuth }) {
  const navigate = useNavigate();
  const toNavigate = (page) => {
    if (page === "login") {
      return navigate("/login");
    }

    if (page === "register") {
      return navigate("/register");
    }

    return;
  };

  return (
    <>
      <Button
        color="primary"
        sx={{
          borderRadius: "8px",
          paddingX: "20px",
          paddingY: "10px",
          ":hover": {
            background: "transparent",
          },
        }}
        onClick={() => toNavigate("register")}
      >
        Sign Up
      </Button>
      <Button
        variant="contained"
        color="primary"
        sx={{
          borderRadius: "8px",
          paddingX: "20px",
          paddingY: "10px",
        }}
        onClick={() => toNavigate("login")}
      >
        Login
      </Button>
    </>
  );
}

GuestMenu.propTypes = {
  setIsAuth: PropTypes.func,
};

export default GuestMenu;
