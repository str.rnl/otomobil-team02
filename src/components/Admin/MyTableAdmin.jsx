import React, { useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TablePagination,
  Button,
  Modal,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import axios from "../../utils/axiosConfig";
import { LoadingButton } from "@mui/lab";
import { useAuth } from "../../contexts/AuthContext";

const columns = [
  { id: "user_id", label: "User ID", minWidth: 50 },
  { id: "nama", label: "Nama", minWidth: 150 },
  { id: "email", label: "Email", minWidth: 200 },
  { id: "role", label: "Role", minWidth: 100 },
  { id: "status", label: "Status", minWidth: 100 },
  { id: "actions", label: "Actions", minWidth: 100 },
];

const themeColor = "#790b0a";

const initialNewUser = {
  users_name: "",
  users_email: "",
  users_role: "",
};

const TableComponent = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [openModal, setOpenModal] = useState(false);
  const [newUser, setNewUser] = useState(initialNewUser);
  const [data, setData] = useState([]);
  const [editIndex, setEditIndex] = useState(-1);

  const [isProgress, setIsProgress] = useState(false);

  const { handleLogout } = useAuth();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleOpenAddModal = () => {
    setOpenModal(true);
    setEditIndex(-1);
    setNewUser(initialNewUser);
  };

  const handleOpenEditModal = (user_id) => {
    setOpenModal(true);
    setEditIndex(user_id);
    const getData = data.find((obj) => obj.users_id === user_id);
    setNewUser(getData);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setNewUser((prevUser) => ({ ...prevUser, [name]: value }));
  };

  const handleAddUser = async () => {
    try {
      const { status } = await axios.post(
        "/api/user",
        {
          users_name: newUser.users_name,
          users_email: newUser.users_email,
          users_role: newUser.users_role.toLowerCase(),
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      );

      if (status === 200) {
        setData((prevData) => [
          ...prevData,
          { ...newUser, users_is_active: false },
        ]);
        setNewUser(initialNewUser);
        setOpenModal(false);
      }
    } catch (error) {
      console.error("add new user failed");
      setNewUser(initialNewUser);
      setOpenModal(false);
    }
  };

  const handleEditUser = async () => {
    try {
      const { status } = await axios.put(
        "/api/update_user",
        {
          users_id: editIndex,
          users_name: newUser.users_name,
          users_email: newUser.users_email,
          users_role: newUser.users_role.toLowerCase(),
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      );

      if (status === 200) {
        setData((prevData) => {
          const newData = [...prevData];
          const index = newData.findIndex(
            (data) => data.users_id === editIndex
          );
          newData[index] = newUser;
          return newData;
        });
        setNewUser(initialNewUser);
        setEditIndex(-1);
        setOpenModal(false);
      }
    } catch (error) {
      console.error("failed to update data");
    }
  };

  const handleToggleStatus = async (user_id, user_is_active) => {
    try {
      setIsProgress(true);
      const { status } = await axios.put(
        "/api/activate_user",
        {
          users_id: user_id,
          users_is_active: user_is_active,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      );

      if (status === 200) {
        setData((prevData) => {
          return prevData.map((user) => {
            return user.users_id === user_id
              ? { ...user, users_is_active: user_is_active }
              : user;
          });
        });

        setIsProgress(false);
      }
    } catch {
      console.error("Activated User is failed");
      setIsProgress(false);
    }
  };

  useEffect(() => {
    const fetchAllUsers = async () => {
      try {
        const { data } = await axios.get("/api/users", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        });
        setData(data);
      } catch (error) {
        if (error.response.status === 401) {
          handleLogout();
          console.error("fetching all users failed");
        }
      }
    };

    fetchAllUsers();
  }, []);

  return (
    <Paper>
      <Button
        variant="contained"
        color="primary"
        onClick={handleOpenAddModal}
        style={{ margin: "20px" }}
      >
        Add User
      </Button>
      <TableContainer>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align="left"
                  style={{
                    minWidth: column.minWidth,
                    backgroundColor: themeColor,
                    color: "#fff",
                  }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => (
                <TableRow key={row.users_id}>
                  <TableCell align="left">{row.users_id}</TableCell>
                  <TableCell align="left">{row.users_name}</TableCell>
                  <TableCell align="left">{row.users_email}</TableCell>
                  <TableCell align="left">{row.users_role}</TableCell>
                  <TableCell align="left">
                    {row.users_is_active ? "Aktif" : "Tidak Aktif"}
                  </TableCell>
                  <TableCell align="left">
                    <Button
                      variant="outlined"
                      onClick={() => handleOpenEditModal(row.users_id)}
                    >
                      Edit
                    </Button>
                    <LoadingButton
                      variant="outlined"
                      loading={isProgress}
                      onClick={() =>
                        handleToggleStatus(row.users_id, !row.users_is_active)
                      }
                      style={{ marginLeft: "10px" }}
                    >
                      {row.users_is_active ? "Unactivated" : "Activated"}
                    </LoadingButton>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
      <Modal
        open={openModal}
        onClose={handleCloseModal}
        aria-labelledby="modal-title"
        aria-describedby="modal-description"
      >
        <Paper
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            padding: "20px",
          }}
        >
          {editIndex >= 0 ? (
            <h2 id="modal-title">Edit User</h2>
          ) : (
            <h2 id="modal-title">Add New User</h2>
          )}
          <TextField
            name="users_name"
            label="Nama"
            value={newUser.users_name}
            onChange={handleInputChange}
            fullWidth
            margin="normal"
            variant="outlined"
          />
          <TextField
            type="email"
            name="users_email"
            label="Email"
            value={newUser.users_email}
            onChange={handleInputChange}
            fullWidth
            margin="normal"
            variant="outlined"
          />
          <FormControl fullWidth margin="normal" variant="outlined">
            <InputLabel id="role-label">Role</InputLabel>
            <Select
              labelId="role-label"
              name="users_role"
              value={newUser.users_role}
              onChange={handleInputChange}
              label="Role"
            >
              <MenuItem value="admin">Admin</MenuItem>
              <MenuItem value="user">User</MenuItem>
            </Select>
          </FormControl>
          {editIndex >= 0 ? (
            <Button
              variant="contained"
              color="primary"
              onClick={handleEditUser}
              style={{ marginTop: "20px" }}
            >
              Save Changes
            </Button>
          ) : (
            <Button
              variant="contained"
              color="primary"
              onClick={handleAddUser}
              style={{ marginTop: "20px" }}
            >
              Add User
            </Button>
          )}
        </Paper>
      </Modal>
    </Paper>
  );
};

export default TableComponent;
