import React, { useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TablePagination,
  Button,
  Modal,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import axios from "../../utils/axiosConfig";
import { LoadingButton } from "@mui/lab";
import { useAuth } from "../../contexts/AuthContext";
import formattedDate from "../../utils/formattedDate";
import formattedRupiah from "../../utils/formattedRupiah";

const columns = [
  { id: "invoice_id", label: "ID", minWidth: 50 },
  { id: "invoice_date", label: "Date", minWidth: 150 },
  { id: "total_course", label: "Total Course", minWidth: 100 },
  { id: "total_price", label: "Total Price", minWidth: 100 },
];

const themeColor = "#790b0a";

const TableComponent = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [data, setData] = useState([]);

  const { handleLogout } = useAuth();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  useEffect(() => {
    const fetchAllInvoice = async () => {
      try {
        const { data } = await axios.get("/get_all_invoice", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        });
        setData(data);
      } catch (error) {
        if (error.response.status === 401) {
          handleLogout();
          console.error("fetching all invoice failed");
        }
      }
    };

    fetchAllInvoice();
  }, []);

  return (
    <Paper>
      {/* <Button
        variant="contained"
        color="primary"
        onClick={handleOpenAddModal}
        style={{ margin: "20px" }}
      >
        Add User
      </Button> */}
      <TableContainer>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align="left"
                  style={{
                    minWidth: column.minWidth,
                    backgroundColor: themeColor,
                    color: "#fff",
                  }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row, index) => (
                <TableRow key={row.invoice_id}>
                  <TableCell align="left">{row.invoice_id}</TableCell>
                  <TableCell align="left">
                    {formattedDate(row.invoice_date)}
                  </TableCell>
                  <TableCell align="left">{row.total_course}</TableCell>
                  <TableCell align="left">
                    IDR {formattedRupiah(row.total_price)}
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

export default TableComponent;
