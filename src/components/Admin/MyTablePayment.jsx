import React, { useEffect, useState } from "react";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  TablePagination,
  Button,
  Modal,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Divider,
} from "@mui/material";
import axios from "../../utils/axiosConfig";
import { LoadingButton } from "@mui/lab";
import { useAuth } from "../../contexts/AuthContext";

const columns = [
  { id: "payment_id", label: "Payment ID", minWidth: 50 },
  { id: "payment_name", label: "Name", minWidth: 150 },
  { id: "payment_number", label: "Number", minWidth: 200 },
  { id: "payment_pic", label: "Logo", minWidth: 100 },
  { id: "status", label: "Status", minWidth: 100 },
  { id: "actions", label: "Actions", minWidth: 100 },
];

const themeColor = "#790b0a";

const initialnewPayment = {
  payment_name: "",
  payment_number: "",
  payment_pic: null,
};

const MyTablePayment = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [openModal, setOpenModal] = useState(false);
  const [newPayment, setNewPayment] = useState(initialnewPayment);
  const [data, setData] = useState([]);
  const [editIndex, setEditIndex] = useState(-1);

  const [isProgress, setIsProgress] = useState(false);

  const { handleLogout } = useAuth();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  const handleOpenAddModal = () => {
    setOpenModal(true);
    setEditIndex(-1);
    setNewPayment(initialnewPayment);
  };

  const handleOpenEditModal = (payment_id) => {
    setOpenModal(true);
    setEditIndex(payment_id);
    const getData = data.find((obj) => obj.payment_id === payment_id);
    setNewPayment(getData);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleInputChange = (event) => {
    const { name, value, files } = event.target;
    if (name !== "payment_pic") {
      setNewPayment({ ...newPayment, [name]: value });
    } else {
      setNewPayment({ ...newPayment, payment_pic: files[0] });
    }

    console.log(newPayment.payment_pic);
  };

  const handleAddPayment = async () => {
    const { payment_name, payment_number, payment_pic } = newPayment;

    const formData = new FormData();
    formData.append("payment_name", payment_name);
    formData.append("payment_number", payment_number);
    formData.append("payment_pic", payment_pic);
    try {
      const { status, data } = await axios.post("/add_payment", formData, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          "Content-Type": "multipart/form-data",
        },
      });

      if (status === 200) {
        setData((prevData) => [
          ...prevData,
          { ...newPayment, payment_is_active: false, payment_pic: data.logo },
        ]);
        setNewPayment(initialnewPayment);
        setOpenModal(false);
      }
    } catch (error) {
      console.error("add new payment failed");
      setNewPayment(initialnewPayment);
      setOpenModal(false);
    }
  };

  const handleEditPayment = async () => {
    try {
      const { status } = await axios.put(
        "/update_payment",
        {
          payment_id: editIndex,
          payment_name: newPayment.payment_name,
          payment_number: newPayment.payment_number,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      );

      if (status === 200) {
        setData((prevData) => {
          const newData = [...prevData];
          const index = newData.findIndex(
            (data) => data.payment_id === editIndex
          );
          newData[index] = newPayment;
          return newData;
        });
        setNewPayment(initialnewPayment);
        setEditIndex(-1);
        setOpenModal(false);
      }
    } catch (error) {
      console.error("failed to update data");
    }
  };

  const handleToggleStatus = async (payment_id, payment_is_active) => {
    try {
      setIsProgress(true);
      const { status } = await axios.put(
        "/activate_payment",
        {
          payment_id: payment_id,
          payment_is_active: payment_is_active,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      );

      if (status === 200) {
        setData((prevData) => {
          return prevData.map((pay) => {
            return pay.payment_id === payment_id
              ? { ...pay, payment_is_active: payment_is_active }
              : pay;
          });
        });

        setIsProgress(false);
      }
    } catch {
      console.error("Activated User is failed");
      setIsProgress(false);
    }
  };

  useEffect(() => {
    const fetchAllPayments = async () => {
      try {
        const { data } = await axios.get("/payments", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        });
        setData(data);
      } catch (error) {
        if (error.response.status === 401) {
          handleLogout();
          console.error("fetching all payments failed");
        }
      }
    };

    fetchAllPayments();
  }, []);

  return (
    <Paper>
      <Button
        variant="contained"
        color="primary"
        onClick={handleOpenAddModal}
        style={{ margin: "20px" }}
      >
        Add Payment
      </Button>
      <TableContainer>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align="left"
                  style={{
                    minWidth: column.minWidth,
                    backgroundColor: themeColor,
                    color: "#fff",
                  }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {data
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => (
                <TableRow key={row.payment_id}>
                  <TableCell align="left">{row.payment_id}</TableCell>
                  <TableCell align="left">{row.payment_name}</TableCell>
                  <TableCell align="left">{row.payment_number}</TableCell>
                  <TableCell align="left">
                    <img
                      width={52}
                      src={`${process.env.REACT_APP_BASE_URL_BE}/${row.payment_pic}`}
                      alt={row.payment_name}
                    />
                  </TableCell>
                  <TableCell align="left">
                    {row.payment_is_active ? "Aktif" : "Tidak Aktif"}
                  </TableCell>
                  <TableCell align="left">
                    <Button
                      variant="outlined"
                      onClick={() => handleOpenEditModal(row.payment_id)}
                    >
                      Edit
                    </Button>
                    <LoadingButton
                      variant="outlined"
                      loading={isProgress}
                      onClick={() =>
                        handleToggleStatus(
                          row.payment_id,
                          !row.payment_is_active
                        )
                      }
                      style={{ marginLeft: "10px" }}
                    >
                      {row.payment_is_active ? "Unactivated" : "Activated"}
                    </LoadingButton>
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 50]}
        component="div"
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
      <Modal
        open={openModal}
        fullWidth
        onClose={handleCloseModal}
        aria-labelledby="modal-title"
        aria-describedby="modal-description"
      >
        <Paper
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            padding: "20px",
          }}
        >
          {editIndex >= 0 ? (
            <h2 id="modal-title">Edit Payment</h2>
          ) : (
            <h2 id="modal-title">Add New Payment</h2>
          )}
          <TextField
            name="payment_name"
            label="Payment Name"
            value={newPayment.payment_name}
            onChange={handleInputChange}
            fullWidth
            margin="normal"
            variant="outlined"
          />
          <TextField
            type="number"
            name="payment_number"
            label="Payment Number"
            value={newPayment.payment_number}
            onChange={handleInputChange}
            fullWidth
            margin="normal"
            variant="outlined"
          />
          <Divider sx={{ marginBottom: "24px" }} />
          {editIndex >= 0 ? (
            ""
          ) : (
            <div style={{ display: "flex", flexDirection: "column" }}>
              <label>Logo</label>
              <input
                type="file"
                name="payment_pic"
                onChange={handleInputChange}
              />
            </div>
          )}

          {editIndex >= 0 ? (
            <Button
              variant="contained"
              color="primary"
              onClick={handleEditPayment}
              style={{ marginTop: "20px" }}
            >
              Save Changes
            </Button>
          ) : (
            <Button
              variant="contained"
              color="primary"
              onClick={handleAddPayment}
              style={{ marginTop: "20px" }}
            >
              Add Payment
            </Button>
          )}
        </Paper>
      </Modal>
    </Paper>
  );
};

export default MyTablePayment;
