import React from "react";

import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import PaymentIcon from "@mui/icons-material/Payment";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import Divider from "@mui/material/Divider";
import Typography from "@mui/material/Typography";
import { Drawer, Toolbar } from "@mui/material";
import { useNavigate } from "react-router-dom";

const Sidebar = ({ drawerWidth, mobileOpen, handleDrawerToggle, window }) => {
  const navigate = useNavigate();

  const container =
    window !== undefined ? () => window().document.body : undefined;

  const menus = [
    {
      title: "Users",
      to: "/admin/users",
      icon: <AccountCircleIcon />,
    },
    {
      title: "Payment Methods",
      to: "/admin/payments",
      icon: <PaymentIcon />,
    },
    {
      title: "Invoices",
      to: "/admin/invoices",
      icon: <PaymentIcon />,
    },
  ];

  const handleNavigate = (to) => {
    navigate(to);
  };

  const drawer = (
    <div>
      <Toolbar>
        <Typography
          sx={{ cursor: "pointer" }}
          onClick={() => navigate("/")}
          variant="h5"
          color="primary"
          fontWeight={600}
        >
          OtoMobil Admin
        </Typography>
      </Toolbar>
      <Divider />
      <List>
        {menus.map((menu, index) => (
          <ListItem
            onClick={() => handleNavigate(menu.to)}
            key={index}
            disablePadding
          >
            <ListItemButton>
              <ListItemIcon>{menu.icon}</ListItemIcon>
              <ListItemText primary={menu.title} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </div>
  );

  return (
    <>
      <Drawer
        container={container}
        variant="temporary"
        open={mobileOpen}
        onClose={handleDrawerToggle}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
        sx={{
          display: { xs: "block", sm: "none" },
          "& .MuiDrawer-paper": {
            boxSizing: "border-box",
            width: drawerWidth,
          },
        }}
      >
        {drawer}
      </Drawer>
      <Drawer
        variant="permanent"
        sx={{
          display: { xs: "none", sm: "block" },
          "& .MuiDrawer-paper": {
            boxSizing: "border-box",
            width: drawerWidth,
          },
        }}
        open
      >
        {drawer}
      </Drawer>
    </>
  );
};

export default Sidebar;
