import React from "react";
import { Grid, Typography, Box, Tooltip, Button } from "@mui/material";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import InstagramIcon from "@mui/icons-material/Instagram";
import YouTubeIcon from "@mui/icons-material/YouTube";
import SendIcon from "@mui/icons-material/Send";
import EmailIcon from "@mui/icons-material/Email";
import { useNavigate } from "react-router-dom";
//import { useItem } from "../../contexts/ItemContext";

export default function FooterItem() {
  //const { categories } = useItem();
  const navigate = useNavigate();
  return (
    <>
      <Grid item xs={12} lg={4}>
        <Typography variant="h6" color="primary" sx={{ marginBottom: "16px" }}>
          About Us
        </Typography>
        <Typography variant="p" component="p" color="text.primary">
          Sed ut perspiciatis unde omnis iste natus error sit voluptatem
          accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae
          ab illo inventore veritatis et quasi architecto beatae vitae dicta
          sunt explicabo.
        </Typography>
      </Grid>
      <Grid item xs={12} lg={4}>
        <Typography variant="h6" color="primary">
          Product
        </Typography>
        <Box sx={{ display: "flex", gap: "16px", marginBottom: "16px" }}>
          <ul style={{ paddingLeft: "32px" }}>
            <li onClick={() => navigate("/mobil?cat_id=1")} className="footer-item">Electric</li>
            <li onClick={() => navigate("/mobil?cat_id=3")} className="footer-item">LCGC</li>
            <li onClick={() => navigate("/mobil?cat_id=5")} className="footer-item">Offroad</li>
            <li onClick={() => navigate("/mobil?cat_id=7")} className="footer-item">SUV</li>
          </ul>
          <ul style={{ paddingLeft: "32px" }}>
            <li onClick={() => navigate("/mobil?cat_id=2")} className="footer-item">Hatchback</li>
            <li onClick={() => navigate("/mobil?cat_id=4")} className="footer-item">MPV</li>
            <li onClick={() => navigate("/mobil?cat_id=6")} className="footer-item">Sedan</li>
            <li onClick={() => navigate("/mobil?cat_id=8")} className="footer-item">Truck</li>
          </ul>
        </Box>
      </Grid>
      <Grid item xs={12} lg={4}>
        <Box sx={{ display: "flex", flexDirection: "column", gap: "16px" }}>
          <Box>
            <Typography
              variant="h6"
              color="primary"
              sx={{ marginBottom: "16px" }}
            >
              Address
            </Typography>
            <Typography variant="p" component="p" color="text.primary">
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque.
            </Typography>
          </Box>
          <Box>
            <Typography
              variant="h6"
              color="primary"
              sx={{ marginBottom: "16px" }}
            >
              Contact Us
            </Typography>
            <Box sx={{ display: "flex", gap: "16px" }}>
              <Tooltip title="Telephone" arrow>
                <Button
                  variant="contained"
                  color="primary"
                  sx={{
                    borderRadius: "50%",
                    width: "64px",
                    height: "64px",
                  }}
                >
                  <LocalPhoneIcon fontSize="large" />
                </Button>
              </Tooltip>
              <Tooltip title="Telephone" arrow>
                <Button
                  variant="contained"
                  color="primary"
                  sx={{
                    borderRadius: "50%",
                    width: "64px",
                    height: "64px",
                  }}
                >
                  <InstagramIcon fontSize="large" />
                </Button>
              </Tooltip>
              <Tooltip title="Telephone" arrow>
                <Button
                  variant="contained"
                  color="primary"
                  sx={{
                    borderRadius: "50%",
                    width: "64px",
                    height: "64px",
                  }}
                >
                  <YouTubeIcon fontSize="large" />
                </Button>
              </Tooltip>
              <Tooltip title="Telephone" arrow>
                <Button
                  variant="contained"
                  color="primary"
                  sx={{
                    borderRadius: "50%",
                    width: "64px",
                    height: "64px",
                  }}
                >
                  <SendIcon fontSize="large" />
                </Button>
              </Tooltip>
              <Tooltip title="Telephone" arrow>
                <Button
                  variant="contained"
                  color="primary"
                  sx={{
                    borderRadius: "50%",
                    width: "64px",
                    height: "64px",
                  }}
                >
                  <EmailIcon fontSize="large" />
                </Button>
              </Tooltip>
            </Box>
          </Box>
        </Box>
      </Grid>
    </>
  );
}
