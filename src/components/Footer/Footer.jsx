import React from "react";
import { Grid, Container } from "@mui/material";
import FooterItem from "./Item/FooterItem";
import { useLocation } from "react-router-dom";

export default function Footer() {
  const { pathname } = useLocation();

  const filtered_pages = [
    "/reset-password",
    "/login",
    "/register",
    "/register/success",
    "/checkout",
    "/checkout/success",
    "/reset-password/success",
    "/admin/users",
    "/admin/payments",
    "/admin/invoices",
  ];

  if (filtered_pages.includes(pathname)) {
    return;
  }

  return (
    <footer
      style={{
        paddingTop: "24px",
        paddingBottom: "24px",
        borderTop: "1px solid #E0E0E0",
      }}
    >
      <Container maxWidth="xl">
        <Grid container spacing={3}>
          <FooterItem />
        </Grid>
      </Container>
    </footer>
  );
}
