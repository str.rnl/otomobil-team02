import { Box, Typography } from "@mui/material";
import React from "react";
import PropTypes from "prop-types";

function Description() {
  return (
    <Box>
      <Typography variant="h5" fontWeight={600} marginBottom="16px">
        Description
      </Typography>
      <Typography
        variant="p"
        component="p"
        fontWeight={400}
        marginBottom="24px"
      >
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Animi quis
        reprehenderit similique libero, deleniti eaque vel provident repellat
        blanditiis hic officia minus dolores consequatur aliquid sequi veritatis
        necessitatibus velit illo quidem eum? Esse cumque sapiente laudantium
        harum mollitia ab maiores, fugit sint ut rerum voluptatibus voluptatum
        voluptas ducimus dignissimos nemo, doloribus, eveniet facilis magni
        nisi! Neque mollitia porro ratione aut earum, ipsum culpa ex amet ipsam
        quasi fugit quis, minus qui veniam, voluptates possimus minima? Ad magni
        nemo voluptatum suscipit nobis! Voluptatem illo, eligendi temporibus
        iste voluptas fugit nobis delectus.
      </Typography>
      <Typography variant="p" component="p" fontWeight={400}>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Totam, nihil
        quia. Quidem laboriosam provident veniam, ipsam molestiae, earum et
        repudiandae modi, facere cum amet quia nobis obcaecati debitis culpa
        saepe cupiditate ad sapiente nesciunt tempora magnam quas fuga
        accusamus! Alias error impedit repudiandae incidunt perspiciatis saepe
        aliquam porro ex mollitia eius, et voluptas dolore optio ipsum non ad
        nesciunt sed minus nostrum voluptatibus cupiditate quae!
      </Typography>
    </Box>
  );
}

Description.propTypes = {
  item: PropTypes.object.isRequired,
};

export default Description;
