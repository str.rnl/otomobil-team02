import {
  Grid,
  Typography,
  Box,
  Select,
  InputLabel,
  MenuItem,
  FormControl,
  Button,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import useFormatRupiah from "../../hooks/useFormatRupiah";
import axios from "../../utils/axiosConfig";
import { useNavigate, useParams } from "react-router-dom";
import { useAuth } from "../../contexts/AuthContext";
import formattedDate from "../../utils/formattedDate";

function Information({ item }) {
  const [schedule, setSchedule] = useState("");
  const [schedules, setSchedules] = useState([]);
  const { isAuth } = useAuth();
  const { id } = useParams();
  const handleChangeSelect = (e) => {
    setSchedule(e.target.value);
  };

  const navigate = useNavigate();

  const userFromLocalStorage = localStorage.getItem("user");
  const dataUser = JSON.parse(userFromLocalStorage);
  let user_id = 0;
  if (userFromLocalStorage) user_id = dataUser.users_id;

  const addToCart = async () => {
    try {
      const payload = {
        fk_users: user_id,
        fk_product: Number(id),
        date: schedule,
      };
      const { status, data } = await axios.post("/api/cart/add_cart", payload, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      });

      if (status === 200) {
        navigate("/checkout");
      }
    } catch (error) {
      console.error("failed to add cart", error);
    }
  };

  const handleClickAddToCart = () => {
    if (isAuth) {
      if (schedule === "") {
        alert("Please select the schedule first.");
      } else {
        addToCart();
      }
    } else {
      navigate("/login");
    }
  };

  useEffect(() => {
    const fetchSchedules = async () => {
      try {
        const { data } = await axios.get("/api/schedule", {
          params: {
            product_id: id,
          },
        });
        setSchedules(data);
      } catch (error) {
        console.error("failed to fetching schedules");
      }
    };

    fetchSchedules();
  }, [id]);

  return (
    <Grid
      container
      sx={{ marginTop: "48px", marginBottom: "40px" }}
      columnSpacing={5}
    >
      <Grid item xs={12} lg={4}>
        <img
          src={process.env.REACT_APP_BASE_URL_BE + "/" + item.product_pic}
          style={{ width: "100%", height: "auto" }}
          alt="mobil-img"
        />
      </Grid>
      <Grid
        item
        xs={12}
        lg={8}
        sx={{ display: "flex", flexDirection: "column" }}
      >
        <Box sx={{ display: "flex", flexDirection: "column", gap: "8px" }}>
          <Typography variant="p" color="grey.600">
            {item?.category?.category_name}
          </Typography>
          <Typography variant="h5" sx={{ fontWeight: "600" }}>
            {item?.product_name}
          </Typography>
          <Typography variant="h5" color="primary" sx={{ fontWeight: "600" }}>
            IDR {useFormatRupiah(item?.product_price)}
          </Typography>
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            mt: "32px",
            flexGrow: 1,
          }}
        >
          <FormControl
            sx={{ width: { xs: "100%", md: "350px" }, marginBottom: "auto" }}
            size="small"
          >
            <InputLabel id="schedule-select">Select Schedule</InputLabel>
            <Select
              labelId="schedule-select"
              id="demo-simple-select"
              value={schedule}
              label="Select Schedule"
              onChange={handleChangeSelect}
            >
              <MenuItem value="">
                <em>None</em>
              </MenuItem>
              {schedules.map((schedule, index) => {
                return (
                  <MenuItem key={index} value={schedule.date}>
                    {formattedDate(schedule.date)}
                  </MenuItem>
                );
              })}
            </Select>
          </FormControl>
          <Box sx={{ display: "flex", gap: "16px" }}>
            <Button
              variant="outlined"
              color="primary"
              onClick={handleClickAddToCart}
              sx={{
                px: "20px",
                py: "10px",
                width: "234px",
                fontWeight: 500,
                borderRadius: "8px",
              }}
            >
              Add to Cart
            </Button>
            <Button
              variant="contained"
              color="primary"
              sx={{
                px: "20px",
                py: "10px",
                width: "234px",
                fontWeight: 500,
                borderRadius: "8px",
              }}
            >
              Buy Now
            </Button>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
}

Information.propTypes = {
  item: PropTypes.object.isRequired,
};

export default Information;
