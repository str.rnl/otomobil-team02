import { Alert, AlertTitle, Box, Collapse, IconButton } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import React from "react";

const MyAlert = ({ isAlert, setIsAlert, alertMsg }) => {
  return (
    <Box sx={{ width: "100%" }}>
      <Collapse in={isAlert}>
        <Alert
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={() => {
                setIsAlert(false);
              }}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          severity={alertMsg.type.toLowerCase()}
          sx={{ marginBottom: "32px" }}
        >
          <AlertTitle>
            <strong>{alertMsg.type}</strong>
          </AlertTitle>
          {alertMsg.msg}
        </Alert>
      </Collapse>
    </Box>
  );
};

export default MyAlert;
