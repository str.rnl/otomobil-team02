import React, { useEffect } from "react";
import Card from "@mui/material/Card";
import Box from "@mui/material/Box";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import useFormatRupiah from "../../hooks/useFormatRupiah";
import { useNavigate, useParams } from "react-router-dom";

const ItemCard = ({ mobil }) => {
  const navigate = useNavigate();
  const params = useParams();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <Box>
      <Card
        key={mobil.product_id}
        sx={{
          maxWidth: "100%",
          boxShadow: "none",
          cursor: "pointer",
          // border:1,
          // borderColor:"red"
        }}
        onClick={() => {
          if (JSON.stringify(params) !== "{}") window.scrollTo(0, 0);
          navigate("/mobil/" + mobil.product_id);
        }}
      >
        <CardMedia
          sx={{ height: 320 }}
          image={process.env.REACT_APP_BASE_URL_BE + "/" + mobil.product_pic}
        />
        <CardContent>
          <Typography
            gutterBottom
            variant="span"
            color="text.secondary"
            component="div"
          >
            {mobil.category.category_name}
          </Typography>
          <Typography
            variant="h6"
            color="text.primary"
            sx={{ fontWeight: 600 }}
          >
            {mobil.product_name}
          </Typography>
        </CardContent>
        <CardActions sx={{ paddingLeft: "16px" }}>
          <Typography variant="h6" color="primary" sx={{ fontWeight: 600 }}>
            IDR {useFormatRupiah(mobil.product_price)}
          </Typography>
        </CardActions>
      </Card>
    </Box>
  );
};

export default ItemCard;
