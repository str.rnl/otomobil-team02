import React from "react";
import ItemCard from "./ItemCard";
import { Box, Container, Grid, Typography } from "@mui/material";
import { useItem } from "../../contexts/ItemContext";
import { useParams, useSearchParams } from "react-router-dom";

const ItemWrapper = () => {
  const { items } = useItem();
  const params = useParams();
  const [searchParams] = useSearchParams();
  const cat_id = searchParams.get("cat_id");
  let listItems = items;
  if (JSON.stringify(params) !== "{}") {
    listItems = items.filter((item) => item.product_id !== Number(params.id));
  }

  if (cat_id) {
    listItems = items.filter((item) => item.fk_category === Number(cat_id));
  }

  if (listItems.length > 0) {
    return (
      <>
        {listItems.map((item, index) => {
          return (
            <Grid
              item
              key={index}
              xs={12}
              md={6}
              lg={4}
              // sx={{border:1, borderColor:"#000000"}}
            >
              <ItemCard mobil={item} />
            </Grid>
          );
        })}
      </>
    );
  }

  return (
    <Container maxWidth="xl">
      <Box sx={{ display: "flex", justifyContent: "center" }}>
        <Typography component="p" fontSize={18}>
          No Item Course
        </Typography>
      </Box>
    </Container>
  );
};

export default ItemWrapper;
