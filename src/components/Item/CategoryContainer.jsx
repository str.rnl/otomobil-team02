import React from "react";
import { Grid, Typography } from "@mui/material";
import PropTypes from "prop-types";
import CategoryWrapper from "../Item/CategoryWrapper";

const ItemContainer = ({ title }) => {
  return (
    <>
      <Typography
        variant="h4"
        color="primary"
        sx={{ fontWeight: 600, 
              textAlign: "center", 
              marginBottom: "80px", 
            }}
      >
        {title}
      </Typography>
      <Grid container spacing={4}>
        <CategoryWrapper />
      </Grid>
    </>
  );
};

ItemContainer.propTypes = {
  title: PropTypes.string.isRequired,
};

export default ItemContainer;
