import React from "react";
import { Grid, Box, Button, Typography } from "@mui/material";
import { useItem } from "../../contexts/ItemContext";
import { useNavigate } from "react-router-dom";

const CategoryWrapper = () => {
  const { categories } = useItem();
  const navigate = useNavigate();
  return (
    <>
      {/* <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }} sx={{border:1, borderColor:"#000000"}}>
        {Array.from(Array(8)).map((_, index) => (
          <Grid item xs={2} sm={3} md={3} key={index}>
            <Typography>xs=2</Typography>
          </Grid>
        ))}
      </Grid> */}
      <Grid
        container
        spacing={{ xs: 2, md: 4 }}
        columns={{ xs: 4, sm: 8, md: 12 }}
        sx={{ justifyContent: "center" }}
      >
        {categories.map((cat) => {
          return (
            <Grid
              key={cat.category_id}
              item
              xs={2}
              sm={3}
              md={3}
              textAlign="center"
              onClick={() => navigate("/mobil?cat_id=" + cat.category_id)}
              // sx={{ border:1, borderColor:"#000000", }}
            >
              <Button sx={{ display: "row" }}>
                <Box>
                  <img
                    src={`${process.env.REACT_APP_BASE_URL_BE}/${cat.category_pic}`}
                    alt={cat.category_name}
                  />
                  <Typography
                    variant="h6"
                    color="text.primary"
                    sx={{ fontWeight: 600 }}
                  >
                    {cat.category_name}
                  </Typography>
                </Box>
              </Button>
            </Grid>
          );
        })}
      </Grid>
    </>
  );
};

export default CategoryWrapper;
