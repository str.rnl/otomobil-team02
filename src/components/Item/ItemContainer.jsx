import React from "react";
import { Grid, Typography } from "@mui/material";
import PropTypes from "prop-types";
import ItemWrapper from "../Item/ItemWrapper";

const CategoryContainer = ({ title }) => {
  return (
    <>
      <Typography
        variant="h4"
        color="primary"
        sx={{ fontWeight: 600, textAlign: "center", marginBottom: "80px" }}
      >
        {title}
      </Typography>
      <Grid container spacing={4}>
        <ItemWrapper />
      </Grid>
    </>
  );
};

CategoryContainer.propTypes = {
  title: PropTypes.string.isRequired,
};

export default CategoryContainer;
