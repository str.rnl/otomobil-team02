import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import PropTypes from "prop-types";
import { matchPath, useLocation } from "react-router-dom";
import formattedDate from "../../utils/formattedDate";
import formattedRupiah from "../../utils/formattedRupiah";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(even)": {
    backgroundColor: theme.palette.primary.light,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

function MyTable({ headerColumns, rows }) {
  const { pathname } = useLocation();
  const matched = matchPath("/invoice/:id", pathname);

  if (rows) {
    return (
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              {headerColumns.map((col, index) => {
                return (
                  <StyledTableCell
                    key={index}
                    sx={{ fontWeight: 600 }}
                    {...col.props}
                  >
                    {col.title}
                  </StyledTableCell>
                );
              })}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row, index) => {
              if (matched) {
                return (
                  <StyledTableRow key={index}>
                    <StyledTableCell component="th" scope="row">
                      {index + 1}
                    </StyledTableCell>
                    <StyledTableCell align="center" width={600}>
                      {row.products.product_name}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {row.products.category.category_name}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      {formattedDate(row.date)}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                      IDR {formattedRupiah(row.products.product_price)}
                    </StyledTableCell>
                  </StyledTableRow>
                );
              }

              return (
                <StyledTableRow key={row.invoice_id}>
                  <StyledTableCell component="th" scope="row">
                    {index + 1}
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    {row.invoice_id}
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    {formattedDate(row.invoice_date)}
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    {row.total_course}
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    IDR {formattedRupiah(row.total_price)}
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    {row.button_details}
                  </StyledTableCell>
                </StyledTableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }

  return;
}

MyTable.propTypes = {
  headerColumn: PropTypes.arrayOf(PropTypes.object).isRequired,
  rows: PropTypes.arrayOf(PropTypes.object),
};

export default MyTable;
