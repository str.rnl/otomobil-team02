import "./App.css";
import { createTheme, ThemeProvider } from "@mui/material";
import Navbar from "./components/Navbar/Navbar";
import AuthProvider from "./contexts/AuthContext";
import Footer from "./components/Footer/Footer";
import MyRoutes from "./routes";
import ItemProvider from "./contexts/ItemContext";
import { useTitle } from "./hooks/useTitle";
import { useLocation } from "react-router-dom";
import { useEffect } from "react";

const theme = createTheme({
  palette: {
    primary: {
      main: "#790b0a",
      light: "rgba(121, 11, 10, 0.10)",
    },
  },
  typography: {
    fontFamily: "Montserrat, sans-serif",
  },
});

function App() {
  const title = useTitle();
  const { pathname } = useLocation();

  document.title = title + " — Otomobil";

  useEffect(() => {
    localStorage.setItem("last_location", pathname);
  });
  // localStorage.setItem("last_location", pathname);

  return (
    <>
      <AuthProvider>
        {/* <ItemContext.Provider value={dataMobil}> */}
        <ItemProvider>
          <ThemeProvider theme={theme}>
            <Navbar />
            <MyRoutes />
            <Footer />
          </ThemeProvider>
        </ItemProvider>
        {/* </ItemContext.Provider> */}
      </AuthProvider>
    </>
  );
}

export default App;
