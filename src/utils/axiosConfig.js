import axios from "axios";

const instance = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL_BE,
});

// instance.interceptors.response.use(
//   (response) => {
//     return response;
//   },
//   (error) => {
//     if (error.response && error.response.status === 401) {
//       localStorage.removeItem("access_token");
//       localStorage.removeItem("user");
//       window.location.href = "/login";
//     }
//   }
// );

export default instance;
