const formattedDate = (inputDate) => {
  const date = new Date(inputDate);
  const options = {
    weekday: "long",
    day: "numeric",
    month: "long",
    year: "numeric",
  };
  return date.toLocaleDateString("en-US", options);
};

export default formattedDate;
