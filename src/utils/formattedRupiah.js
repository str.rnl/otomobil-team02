const formattedRupiah = (number) => {
  const formattedValue = new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    currencyDisplay: "narrowSymbol", // Menghilangkan "Rp"
    maximumFractionDigits: 0, // Menghilangkan angka di belakang koma
  }).format(number);

  return formattedValue.replace(/Rp\s?/, "");
};

export default formattedRupiah;
