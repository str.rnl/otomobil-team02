import { createContext, useState, useContext } from "react";

const CheckoutContext = createContext(null);

const CheckoutProvider = ({ children }) => {
  const init = {
    payment_name: "no payment available",
    payment_number: "0000000000",
    payment_pic: "images/no_pay.png",
  };

  const [payment, setPayment] = useState(init);
  return (
    <CheckoutContext.Provider value={{ payment, setPayment }}>
      {children}
    </CheckoutContext.Provider>
  );
};

export default CheckoutProvider;

export function useCheckout() {
  return useContext(CheckoutContext);
}
