import { createContext, useContext, useEffect, useState } from "react";
import axios from "../utils/axiosConfig";

const ItemContext = createContext(null);

const ItemProvider = ({ children }) => {
  // const dataMobil = [
  //   // {
  //   //   product_id: 1,
  //   //   Product_name: "Kijang Innova",
  //   //   product_price: 700000,
  //   //   product_description:"old but gold"
  //   //   product_pic: MobilImage,
  //   //   fk_category: 4
  //   // },
  //   // sementara pakai yang di bawah ini
  //   {
  //     id: 1,
  //     name: "Kijang Innova",
  //     category: "SUV",
  //     price: "700.000",
  //     desc: "kijang inova putih bersih, cocok untuk pemula",
  //     pic: MobilImage,
  //   },
  //   {
  //     id: 2,
  //     name: "Honda Brio",
  //     category: "LCGC",
  //     price: "500.000",
  //     desc: "mobil kecil gesit bisa masuk gang sempit",
  //     pic: MobilImage2,
  //   },
  //   {
  //     id: 3,
  //     name: "Hyundai Palisade 2021",
  //     category: "SUV",
  //     price: "800.000",
  //     desc: "SUV mewah berwarna coklat bawaan dari jepang",
  //     pic: MobilImage3,
  //   },
  //   {
  //     id: 4,
  //     name: "Mitsubitshi Pajero",
  //     category: "SUV",
  //     price: "800.000",
  //     desc: "mobil elit tanjakan tidak sulit",
  //     pic: MobilImage4,
  //   },
  //   {
  //     id: 5,
  //     name: "Dump Truck for Mining Constructor",
  //     category: "Truck",
  //     price: "1.200.000",
  //     desc: "memiliki roda sebesar kolam renang",
  //     pic: MobilImage5,
  //   },
  //   {
  //     id: 6,
  //     name: "Sedan Honda Civic",
  //     category: "Sedan",
  //     price: "400.000",
  //     desc: "nyetir santai dan elegan pakai sedan ini",
  //     pic: MobilImage6,
  //   },
  // ];

  const [items, setItems] = useState([]);

  const [itemDetail, setItemDetail] = useState({});

  const [categories, setCategories] = useState([]);

  const [category, setCategory] = useState({});

  const fetchItemDetail = async (id) => {
    try {
      const { data } = await axios.get("api/product/get_detail_product", {
        params: {
          id: id,
        },
      });
      setItemDetail(data);
    } catch (error) {
      console.error("error fetching data");
    }
  };

  const fetchListItem = async (id) => {
    try {
      const { data } = await axios.get("api/product/get_category", {
        params: {
          id: id,
        },
      });
      setCategory(data[0]);
    } catch (error) {
      console.error("error fetching data");
    }
  };

  useEffect(() => {
    const getItem = async () => {
      try {
        const { data } = await axios.get("/api/product/get_products");
        setItems(data);
      } catch (error) {
        console.error("error fetching data");
      }
    };

    const getCategories = async () => {
      try {
        const { data } = await axios.get("/api/product/get_category");
        setCategories(data);
      } catch (error) {
        console.error("error fetching data");
      }
    };

    getItem();
    getCategories();
  }, []);

  return (
    <ItemContext.Provider
      value={{
        items,
        setItems,
        fetchItemDetail,
        itemDetail,
        categories,
        category,
        fetchListItem,
      }}
    >
      {children}
    </ItemContext.Provider>
  );
};

export default ItemProvider;

export function useItem() {
  return useContext(ItemContext);
}
