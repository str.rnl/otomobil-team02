import { matchPath, useLocation } from "react-router-dom";
import { routes } from "../routes";

const useTitle = () => {
  const { pathname } = useLocation();
  //console.log(pathname);
  const route = routes.find((obj) => {
    //console.log(obj.path);
    const matchRoute = matchPath(obj.path, pathname);
    if (matchRoute) return matchRoute.pattern.path === obj.path;
  });
  return route.name;
};

export { useTitle };
