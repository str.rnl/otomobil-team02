import { useState, useEffect } from "react";

// Fungsi untuk mengubah angka menjadi format Rupiah
const formatToRupiah = (number) => {
  const formattedValue = new Intl.NumberFormat("id-ID", {
    style: "currency",
    currency: "IDR",
    currencyDisplay: "narrowSymbol", // Menghilangkan "Rp"
    maximumFractionDigits: 0, // Menghilangkan angka di belakang koma
  }).format(number);

  return formattedValue.replace(/Rp\s?/, "");
};

// Custom hook untuk mengubah angka menjadi format Rupiah
const useFormatRupiah = (initialValue) => {
  const [formattedValue, setFormattedValue] = useState(
    formatToRupiah(initialValue)
  );

  useEffect(() => {
    setFormattedValue(formatToRupiah(initialValue));
  }, [initialValue]);

  return formattedValue;
};

export default useFormatRupiah;
