const useCheckValue = (obj) => {
  return Object.values(obj).every(
    (value) => typeof value === "string" && value.trim() === ""
  );
};

export { useCheckValue };
