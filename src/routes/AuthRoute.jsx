import { Navigate } from "react-router-dom";

export default function AuthRoute({ children }) {
  const token = localStorage.getItem("access_token");
  const lastLocation = localStorage.getItem("last_location");
  if (token) {
    if (lastLocation) {
      return <Navigate to={lastLocation} />;
    }

    return <Navigate to="/" />;
  }

  return children;
}
