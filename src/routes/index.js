import Confirmation from "../pages/Auth/Confirmation";
import Login from "../pages/Auth/Login";
import Register from "../pages/Auth/Register";
import DetailMobil from "../pages/DetailMobil/DetailMobil";
import LandingPage from "../pages/LandingPage/LandingPage";
import ListMenuMobil from "../pages/ListMenuMobil/ListMenuMobil";
import ResetPassword from "../pages/ResetPassword/ResetPassword";
import MyClass from "../pages/MyClass/MyClass";
import Checkout from "../pages/Checkout/Checkout";
import CheckoutSuccess from "../pages/Checkout/SuccessPayment/SuccessPayment";
import Invoice from "../pages/Invoice/Invoice";
import { useRoutes } from "react-router-dom";
import DetailInvoice from "../pages/Invoice/Detail/DetailInvoice";
import PrivateRoute from "./PrivateRoute";
import AuthRoute from "./AuthRoute";
import SuccessConfirm from "../pages/ResetPassword/SuccessConfirm";
import UserAdmin from "../pages/Admin/UserAdmin";
import PaymentAdmin from "../pages/Admin/PaymentAdmin";
import InvoiceAdmin from "../pages/Admin/InvoiceAdmin";

const routes = [
  {
    path: "/",
    element: <LandingPage />,
    exact: true,
    name: "Home",
  },
  {
    path: "/login",
    element: (
      <AuthRoute>
        <Login />
      </AuthRoute>
    ),
    exact: false,
    name: "Login",
  },
  {
    path: "/register",
    element: (
      <AuthRoute>
        <Register />
      </AuthRoute>
    ),
    exact: false,
    name: "Sign Up",
  },
  {
    path: "/register/success",
    element: (
      <AuthRoute>
        <Confirmation />
      </AuthRoute>
    ),
    exact: false,
    name: "",
  },
  {
    path: "/reset-password",
    element: (
      <AuthRoute>
        <ResetPassword />
      </AuthRoute>
    ),
    exact: false,
    name: "Reset Password",
  },
  {
    path: "/reset-password/success",
    element: (
      <AuthRoute>
        <SuccessConfirm />
      </AuthRoute>
    ),
    exact: false,
    name: "Reset Password",
  },
  {
    path: "/mobil",
    element: <ListMenuMobil />,
    exact: false,
    name: "Mobil",
  },
  {
    path: "/mobil/:id",
    element: <DetailMobil namePath="Detail Mobil" />,
    exact: false,
    name: "Details Mobil",
  },
  {
    path: "/invoice",
    element: (
      <PrivateRoute>
        <Invoice namePath="Invoice" />
      </PrivateRoute>
    ),
    exact: false,
    name: "Invoice",
  },
  {
    path: "/invoice/:id",
    element: (
      <PrivateRoute>
        <DetailInvoice />
      </PrivateRoute>
    ),
    exact: false,
    name: "Details Invoice",
  },
  {
    path: "/myclass",
    element: (
      <PrivateRoute>
        <MyClass namePath="myclass" />
      </PrivateRoute>
    ),
    exact: false,
    name: "My Class",
  },
  {
    path: "/myclass/:id",
    element: (
      <PrivateRoute>
        <MyClass />
      </PrivateRoute>
    ),
    exact: false,
    name: "Detail Class",
  },
  {
    path: "/checkout",
    element: (
      <PrivateRoute>
        <Checkout />
      </PrivateRoute>
    ),
    exact: false,
    name: "Checkout",
  },
  {
    path: "/checkout/success",
    element: (
      <PrivateRoute>
        <CheckoutSuccess />
      </PrivateRoute>
    ),
    exact: false,
    name: "Success Payment",
  },
  {
    path: "/admin/users",
    element: (
      <PrivateRoute>
        <UserAdmin />
      </PrivateRoute>
    ),
    exact: false,
    name: "Users Admin",
  },
  {
    path: "/admin/payments",
    element: (
      <PrivateRoute>
        <PaymentAdmin />
      </PrivateRoute>
    ),
    exact: false,
    name: "Payments Admin",
  },
  {
    path: "/admin/invoices",
    element: (
      <PrivateRoute>
        <InvoiceAdmin />
      </PrivateRoute>
    ),
    exact: false,
    name: "Invoices Admin",
  },
];
const MyRoutes = () => {
  const routesData = useRoutes(routes);

  return <>{routesData}</>;
};

export default MyRoutes;

export { routes };
