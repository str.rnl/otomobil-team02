import {
  Box,
  Container,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from "@mui/material";
import React, { useEffect, useState } from "react";
import MyBreadcrumbs from "../../../components/MyBreadcrumbs/MyBreadcrumbs";
import { useLocation, useParams } from "react-router-dom";
import MyTable from "../../../components/Table/MyTable";
import formattedDate from "../../../utils/formattedDate";
import axios from "../../../utils/axiosConfig";
import formattedRupiah from "../../../utils/formattedRupiah";

const DetailInvoice = () => {
  const { id } = useParams();
  const { state } = useLocation();

  const [invoiceDetail, setInvoiceDetail] = useState([]);

  const headerData = [
    {
      title: "No",
      props: {},
    },
    {
      title: "Course Name",
      props: {
        align: "center",
      },
    },
    {
      title: "Type",
      props: {
        align: "center",
      },
    },
    {
      title: "Schedule",
      props: {
        align: "center",
      },
    },
    {
      title: "Price",
      props: {
        align: "center",
      },
    },
  ];

  const rowData = [
    {
      no: 1,
      course_name: "Hyundai Palisade 2021",
      type: "SUV",
      schedule: "Wednesday, 27 July 2022",
      price: "IDR 850.000",
    },
  ];

  useEffect(() => {
    const fetchDetailInvoice = async () => {
      try {
        const { data } = await axios.get("/get_detail_invoice", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
          params: {
            fk_order: id,
          },
        });

        if (data) {
          setInvoiceDetail(data);
        }
      } catch (error) {
        console.error("failed fetching data carts", error);
      }
    };

    fetchDetailInvoice();
  }, []);

  return (
    <Container maxWidth="xl" sx={{ marginBottom: "207px" }}>
      <Box sx={{ marginBottom: "32px" }}>
        <MyBreadcrumbs />
      </Box>
      <Box>
        <Typography
          variant="h5"
          color="text.secondary"
          sx={{ fontWeight: 600, marginBottom: "24px" }}
        >
          Details Invoice
        </Typography>
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "end",
            marginBottom: "42px",
          }}
        >
          <Box width={"300px"}>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell
                    sx={{ padding: 0, fontSize: "16px", border: 0 }}
                    align="left"
                  >
                    No Invoice :
                  </TableCell>
                  <TableCell
                    sx={{ padding: 0, fontSize: "16px", border: 0 }}
                    align="left"
                  >
                    &nbsp;
                    {id}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell
                    sx={{ padding: 0, fontSize: "16px", border: 0 }}
                    align="left"
                  >
                    Date :
                  </TableCell>
                  <TableCell
                    sx={{ padding: 0, fontSize: "16px", border: 0 }}
                    align="left"
                  >
                    {formattedDate(state?.invoice_date)}
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Box>
          <Box>
            <Typography
              component="p"
              sx={{ fontSize: "18px", fontWeight: 600, marginRight: "24px" }}
            >
              Total Price{" "}
              <Typography variant="span">
                IDR {formattedRupiah(state?.total_price)}
              </Typography>
            </Typography>
          </Box>
        </Box>
        <MyTable headerColumns={headerData} rows={invoiceDetail} />
      </Box>
    </Container>
  );
};

export default DetailInvoice;
