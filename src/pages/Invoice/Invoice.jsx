import { Box, Button, Container, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import MyBreadcrumbs from "../../components/MyBreadcrumbs/MyBreadcrumbs";
import MyTable from "../../components/Table/MyTable";
import { useNavigate } from "react-router-dom";
import axios from "../../utils/axiosConfig";

const Invoice = () => {
  const navigate = useNavigate();

  const toDetails = (invoice_id, invoice_date, total_price) => {
    navigate("/invoice/" + invoice_id, {
      state: { invoice_date, total_price },
    });
  };

  const headerData = [
    {
      title: "No",
      props: {},
    },
    {
      title: "No Invoice",
      props: {
        align: "center",
      },
    },
    {
      title: "Date",
      props: {
        align: "center",
      },
    },
    {
      title: "Total Course",
      props: {
        align: "center",
      },
    },
    {
      title: "Total Price",
      props: {
        align: "center",
      },
    },
    {
      title: "Action",
      props: {
        align: "center",
      },
    },
  ];

  const [invoice, setInvoice] = useState([]);
  const userFromLocalStorage = localStorage.getItem("user");
  const dataUser = JSON.parse(userFromLocalStorage);
  let user_id = 0;
  if (userFromLocalStorage) user_id = dataUser.users_id;

  const rowData = [
    {
      no: 1,
      no_invoice: "OT00003",
      date: "12 July 2022",
      total_course: 1,
      total_price: "IDR 850.000",
      button_details: (
        <Button
          fullWidth
          variant="outlined"
          sx={{
            fontWeight: 600,
            borderWidth: "2px",
            borderRadius: "8px",
            transition: "none",
            "&:hover": {
              borderWidth: "2px",
            },
          }}
          size="lg"
          color="primary"
          onClick={() => toDetails("OT00003")}
        >
          Details
        </Button>
      ),
    },
    {
      no: 2,
      no_invoice: "OT00002",
      date: "05 February 2022",
      total_course: 1,
      total_price: "IDR 400.000",
      button_details: (
        <Button
          fullWidth
          variant="outlined"
          sx={{
            fontWeight: 600,
            borderWidth: "2px",
            borderRadius: "8px",
            transition: "none",
            "&:hover": {
              borderWidth: "2px",
            },
          }}
          size="lg"
          color="primary"
          onClick={() => toDetails("OT00002")}
        >
          Details
        </Button>
      ),
    },
    {
      no: 3,
      no_invoice: "OT00001",
      date: "30 August 2021",
      total_course: 1,
      total_price: "IDR 600.000",
      button_details: (
        <Button
          fullWidth
          variant="outlined"
          sx={{
            fontWeight: 600,
            borderWidth: "2px",
            borderRadius: "8px",
            transition: "none",
            "&:hover": {
              borderWidth: "2px",
            },
          }}
          size="lg"
          color="primary"
          onClick={() => toDetails("OT00001")}
        >
          Details
        </Button>
      ),
    },
  ];

  useEffect(() => {
    const fetchInvoice = async () => {
      try {
        const { data } = await axios.get("/get_invoice", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
          params: {
            user_id: user_id,
          },
        });

        if (data) {
          const datas = data.map((obj) => ({
            ...obj,
            button_details: (
              <Button
                fullWidth
                variant="outlined"
                sx={{
                  fontWeight: 600,
                  borderWidth: "2px",
                  borderRadius: "8px",
                  transition: "none",
                  "&:hover": {
                    borderWidth: "2px",
                  },
                }}
                size="lg"
                color="primary"
                onClick={() =>
                  toDetails(obj.invoice_id, obj.invoice_date, obj.total_price)
                }
              >
                Details
              </Button>
            ),
          }));
          setInvoice(datas);
        }
      } catch (error) {
        console.error("failed fetching data carts", error);
      }
    };

    fetchInvoice();
  }, []);

  return (
    <Container maxWidth="xl" sx={{ marginBottom: "207px" }}>
      <Box sx={{ marginBottom: "32px" }}>
        <MyBreadcrumbs />
      </Box>
      <Box>
        <Typography
          variant="h5"
          color="text.secondary"
          sx={{ fontWeight: 600, marginBottom: "24px" }}
        >
          Menu Invoice
        </Typography>
        <MyTable headerColumns={headerData} rows={invoice} />
      </Box>
    </Container>
  );
};

export default Invoice;
