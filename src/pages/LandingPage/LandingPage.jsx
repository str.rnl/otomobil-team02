import React from "react";
import { Container, Grid, Box, Typography, CardContent } from "@mui/material";
import Cover from "../../assets/cover.jpg";
import Desc from "../../assets/cover_desc.png";
import ItemContainer from "../../components/Item/ItemContainer";
import CategoryContainer from "../../components/Item/CategoryContainer";

export default function LandingPage() {
  // const { pathname } = useLocation();
  const cover_desc = [
    {
      id: 1,
      total: "50+",
      description: "A class ready to make you a reliable driver",
    },

    {
      id: 1,
      total: "20+",
      description: "Professional workforce with great experience",
    },

    {
      id: 1,
      total: "10+",
      description: "Cooperate with driver service partners",
    },
  ];
  return (
    <>
    <Grid
            container
            spacing={0}
            direction="column"
            alignItems="center"
            sx={{
                // border: 1,
                // borderColor:"black",
                width: "100%",
                minHeight: "500px",
            }}
      >
      <Box
        sx={{
          display: "flex",
          backgroundImage: `url(${Cover})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          alignItems: "center",
          minHeight: "520px",
          width: "100%",
          top: "86px",
          marginBottom: "81px",
        }}
      >
        <Box
          sx={{
            flexGrow: 1,
          }}
        >
          <Typography
            sx={{
              textAlign: "center",
              fontSize: {
                  xs: "24px",
                  sm: "24px",
                  md: "24px",
                  lg: "28px",
                  xl: "32px",
                },
              fontWeight: "600",
              fontFamily: "Montserrat",
              color: "#ffffff",
              marginTop: "20px",
            }}
          >
            We provide driving lessons for various types of cars
          </Typography>
          <Typography
            sx={{
              textAlign: "center",
               fontSize: {
                  xs: "16px",
                  sm: "16px",
                  md: "16px",
                  lg: "20px",
                  xl: "24px",
                },
              fontWeight: "400",
              fontFamily: "Montserrat",
              color: "#ffffff",
            }}
          >
            Professional staff who are ready to help you to become a
            much-needed reliable driver
          </Typography>
          <Grid
            container
            direction="row"
            justifyContent="space-evenly"
            sx={
              {
                // border:1,
                // borderColor:"#ffffff",
              }
            }
          >
            {cover_desc.map((cover) => {
              return (
                <Box
                  key={cover.id}
                  sx={{
                    display: "flex",
                    alignContent: "center",
                    // border: 1,
                    // borderColor:"#ffffff",
                    textAlign: "center",
                  }}
                >
                  <CardContent>
                    <Typography
                      gutterBottom
                      variant="h5"
                      sx={{
                        // border: 1,
                        // borderColor:"#ffffff",
                        fontSize: {
                            xs: "36px",
                            sm: "36px",
                            md: "36px",
                            lg: "42px",
                            xl: "48px",
                          },
                        fontWeight: "600",
                        fontFamily: "Montserrat",
                        color: "#ffffff",
                        width: "100%",
                        height: "60px",
                      }}
                    >
                      {cover.total}
                    </Typography>
                    <Typography
                      sx={{
                        // border: 1,
                        // borderColor:"#ffffff",
                        textAlign: "center",
                        fontSize: {
                            xs: "14px",
                            sm: "14px",
                            md: "14px",
                            lg: "14px",
                            xl: "16px",
                          },
                        fontWeight: "500",
                        fontFamily: "Montserrat",
                        color: "#ffffff",
                        width: "295px",
                        height: "85px",
                      }}
                    >
                      {cover.description}
                    </Typography>
                  </CardContent>
                </Box>
              );
            })}
          </Grid>
        </Box>
      </Box>

      <Container maxWidth="xl" sx={{ marginBottom: "150px" }}>
        <ItemContainer title="Join us for the course" />
      </Container>

      <Container maxWidth="xl" sx={{ marginBottom: "72px" }}>
        <Grid
          container
          spacing={0}
          sx={{
             display: "flex",
              alignItems: "center",
          }}
        >
          <Grid item xs={12} md={8}>
              <Box
                sx={
                  {
                    // border:1,
                    // borderColor:"#000000",
                  }
                }
              >
            <Typography
              sx={{
                textAlign: "left",
                fontSize: "40px",
                fontWeight: "600",
                fontFamily: "Montserrat",
                color: "#790B0A",
                // border:1, 
                // borderColor:"#000000",
              }}
            >
              Gets your best benefit
            </Typography>
            <Typography
              sx={{
                textAlign: "justify",
                fontSize: "16px",
                fontWeight: "500",
                fontFamily: "Montserrat",
                // width:'80%',
                color: "#000000",
                marginTop: "20px",
                // border:1, 
                // borderColor:"#000000",
              }}
            >
               ed ut perspiciatis unde omnis iste natus error sit voluptatem
               accusantium doloremque laudantium, totam rem aperiam, eaque
               ipsa quae ab illo inventore veritatis et quasi architecto
               beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem
               quia voluptas sit aspernatur aut odit aut
            </Typography>
          </Box>
          </Grid>

            <Grid item xs={12} md={4}>
              <Box sx={{ textAlign: { xs: "center", lg: "right" } }}>
          <img
              src={Desc}
              alt="description"
              sx={{
                // width: "373px",
                // height: "270px",
                textAlign: "right",
              }}
            />
            </Box>
            </Grid>
        </Grid>
      </Container>

      <Container maxWidth="xl" sx={{ marginBottom: "150px" }}>
        <CategoryContainer title="More car type you can choose" />
      </Container>
    </Grid>
    </>
  );
}
