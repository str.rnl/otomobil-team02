import { Box, Container, Grid, Skeleton } from "@mui/material";
import ItemContainer from "../../components/Item/ItemContainer";
import { useParams } from "react-router-dom";
import { Suspense, lazy, useEffect } from "react";
import { useItem } from "../../contexts/ItemContext";

const Information = lazy(() =>
  import("../../components/DetailMobil/Information")
);
const Description = lazy(() =>
  import("../../components/DetailMobil/Description")
);

const LoadingDataInformation = () => {
  return (
    <Grid
      container
      sx={{ marginTop: "48px", marginBottom: "40px" }}
      columnSpacing={5}
    >
      <Grid item xs={12} lg={4}>
        <Skeleton
          variant="rectangular"
          animation="wave"
          width={"100%"}
          height={250}
        ></Skeleton>
      </Grid>
      <Grid
        item
        xs={12}
        lg={8}
        sx={{ display: "flex", flexDirection: "column" }}
      >
        <Box sx={{ display: "flex", flexDirection: "column", gap: "8px" }}>
          <Skeleton
            variant="rectangular"
            animation="wave"
            width={"100%"}
            height={20}
          ></Skeleton>
          <Skeleton
            variant="rectangular"
            animation="wave"
            width={"100%"}
            height={20}
          ></Skeleton>
          <Skeleton
            variant="rectangular"
            animation="wave"
            width={"100%"}
            height={20}
          ></Skeleton>
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            mt: "32px",
            flexGrow: 1,
          }}
        >
          <Box sx={{ marginBottom: "auto" }}>
            <Skeleton
              variant="rectangular"
              animation="wave"
              width={"35%"}
              height={35}
            ></Skeleton>
          </Box>

          <Box sx={{ display: "flex", gap: "16px" }}>
            <Skeleton
              variant="rectangular"
              animation="wave"
              width={"234px"}
              height={35}
            ></Skeleton>
            <Skeleton
              variant="rectangular"
              animation="wave"
              width={"234px"}
              height={35}
            ></Skeleton>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};

const LoadingDataDescription = () => {
  return (
    <Box sx={{ display: "flex", flexDirection: "column", gap: "16px" }}>
      <Skeleton
        variant="rectangular"
        animation="wave"
        width={"100%"}
        height={20}
      ></Skeleton>
      <Skeleton
        variant="rectangular"
        animation="wave"
        width={"100%"}
        height={20}
      ></Skeleton>
      <Skeleton
        variant="rectangular"
        animation="wave"
        width={"100%"}
        height={20}
      ></Skeleton>
      <Skeleton
        variant="rectangular"
        animation="wave"
        width={"100%"}
        height={20}
      ></Skeleton>
    </Box>
  );
};

export default function DetailMobil() {
  const { id } = useParams();
  const { itemDetail, fetchItemDetail } = useItem();
  useEffect(() => {
    fetchItemDetail(id);
  }, [id]);

  return (
    <>
      <Container maxWidth="xl" sx={{ marginBottom: "80px" }}>
        <Suspense fallback={<LoadingDataInformation />}>
          <Information item={itemDetail} />
        </Suspense>
        <Suspense fallback={<LoadingDataDescription />}>
          <Description item={itemDetail} />
        </Suspense>
      </Container>
      <Box height="1px" sx={{ background: "#E0E0E0", border: "none" }}></Box>
      <Container
        maxWidth="xl"
        sx={{ marginTop: "80px", marginBottom: "100px" }}
      >
        <ItemContainer title="Another favourite course" />
      </Container>
    </>
  );
}
