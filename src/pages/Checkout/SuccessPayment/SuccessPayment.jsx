import { Box, Button, Typography } from "@mui/material";
import SuccessIllustration from "../../../assets/success_illustration.svg";
import { useNavigate } from "react-router";
import arrow from "../../../assets/arrow .png";
import house from "../../../assets/house .png";

export default function SuccessPayment() {
    const navigate = useNavigate();
    const toNavigate = (page) =>{
        if (page === "invoice") {
            return navigate("/invoice");
          }

        if (page === "home") {
            return navigate("/");
        }
    };


    return(
        <>
      <Box
        width="fullWidth"
        sx={{
          position: "relative",
        }}
      >
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, 50%)",
            textAlign: "center",
            width: "90%",
          }}
        >
          <Box>
            <img src={SuccessIllustration} alt="icon-success" />
          </Box>
          <Box>
            <Typography
              variant="h5"
              color="primary"
              sx={{ fontWeight: "500", marginBottom: "8px" }}
            >
              Purchased Successfully
            </Typography>
            <Typography
              component="p"
              color="text.secondary"
              sx={{ marginBottom: "40px" }}
            >
              That's Great! We're ready fo driving day
            </Typography>

            <Button
              variant="outlined"
              sx={{ 
                padding: "16px 24px",
                marginRight:'30px', 
              }}
              onClick={() => toNavigate("home")}
            >
              <img
                    src={house}
                    alt="trash"
                    width="20px"
                    height="20px"
                    sx={{
                      // border: 2,
                      // borderColor:"blue",
                      component: "button",
                    }}
                  ></img>
              <Typography component="span" sx={{ fontWeight: 600, marginLeft:"5px", }}>
                Back to Home
              </Typography>
            </Button>
            <Button
              variant="contained"
              color="primary"
              sx={{ padding: "16px 24px" }}
              onClick={() => toNavigate("invoice")}
            >
              <img
                    src={arrow}
                    alt="trash"
                    width="20px"
                    height="20px"
                    sx={{
                      // border: 2,
                      // borderColor:"blue",
                      component: "button",
                    }}
                  ></img>
              <Typography component="span" sx={{ fontWeight: 600, marginLeft:"5px" }}>
                Open Invoice
              </Typography>
            </Button>

          </Box>
        </Box>
      </Box>
    </>
    )

}