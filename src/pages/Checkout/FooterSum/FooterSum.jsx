import React, { useState } from "react";
import {
  Box,
  Grid,
  Container,
  Button,
  Typography,
  ToggleButton,
  ToggleButtonGroup,
} from "@mui/material";
import { useNavigate } from "react-router";
//import PaymentMethod from "../../components/Modal/PaymentMethod"
import Modal from "@mui/material/Modal";
import formattedRupiah from "../../../utils/formattedRupiah";
import axios from "../../../utils/axiosConfig";

export default function FooterSum({ user_id, data_based_id, payment_method }) {
  //const {payment, setPayment} = useCheckout()
  const [view, setView] = useState("");

  // const payment_method = [
  //   {
  //     payment_id: 1,
  //     payment_name: "OvO",
  //     payment_number: "1111111111",
  //     payment_pic: "images/OVO.png",
  //   },
  //   {
  //     payment_id: 2,
  //     payment_name: "Gopay",
  //     payment_number: "2222222222",
  //     payment_pic: "images/Gopay.png",
  //   },
  // ];

  const handleChange = (currView) => {
    setView(currView);
  };

  const styleh = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: {
      xs: 200,
      sm: 200,
      md: 200,
      lg: 350,
      xl: 400,
    },
    textAlign:"center",
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  let data = data_based_id;

  const datas = data.map((item) => {
    return {
      cart_id: item.cart_id,
      detail_invoice_schedule: item.date,
      detail_invoice_price: item.products.product_price,
      fk_product: item.fk_product,
    };
  });

  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    if (data.length > 0) {
      setOpen(true);
      console.log(datas);
    } else {
      //setOpen(true);
      alert("Please select item first.");
    }
  };
  const handleClose = () => setOpen(false);
  const total = data.reduce(
    (sum, currentVal) => (sum = sum + currentVal.products.product_price),
    0
  );

  const proceedCheckout = async () => {
    try {
      const { status } = await axios.post(
        "/add_invoice",
        {
          fk_payment: Number(view),
          fk_users: Number(user_id),
          invoice_details: datas,
        },
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        }
      );

      if (status === 200) {
        toNavigate("success");
      }
    } catch (error) {
      console.error("failed to proceed checkout", error.response);
    }
  };

  const navigate = useNavigate();
  const toNavigate = (page) => {
    if (page === "success") {
      return navigate("/checkout/success");
    }
  };

  return (
    <>
      <Box
        alignItems="center"
        sx={{
          paddingTop: "24px",
          paddingBottom: "24px",
          // borderTop: "1px solid #E0E0E0",
          borderTop: 1,
          borderColor: "#a6abb3",
          width: "100%",
        }}
      >
        <Container maxWidth="xl">
          <Grid
            container
            spacing={0}
            justifyContent="space-between"
            sx={
              {
                // border: 1,
                // borderColor:"blue",
              }
            }
          >
            {/* {data.map((total) => {
              return ( */}
            <Box
              justifyItems="start"
              alignItems="center"
              sx={{
                display: "flex",
                flexGrow: 1,
                // border: 1,
                // borderColor:"primary",
              }}
            >
              <Typography
                gutterBottom
                variant="span"
                color="text.secondary"
                component="div"
                sx={{
                  // fontWeight: 600,
                  // marginLeft: "10px",
                }}
              >
                Total:
              </Typography>
              <Typography
                variant="h6"
                color="primary"
                sx={{
                  fontWeight: 600,
                  marginLeft: "10px",
                  marginBottom:"5px",
                }}
              >
                IDR {formattedRupiah(total)}
              </Typography>
            </Box>
            {/* );
            })} */}
            <Button
              variant="contained"
              color="primary"
              sx={{
                borderRadius: "8px",
                width: "200px",
                paddingX: "20px",
                paddingY: "10px",
              }}
              // onClick={() => toNavigate("success")}
              onClick={handleOpen}
            >
              Pay Now
            </Button>
          </Grid>
        </Container>
      </Box>
      <Modal
        open={open}
        //onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={styleh}>
          <Typography
            id="modal-modal-title"
            variant="h6"
            component="h2"
            sx={{ marginBottom: "16px" }}
          >
            Select Payment Method
          </Typography>
          {/* <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            Belum tersedia metode pembelian apapun
          </Typography> */}
          <ToggleButtonGroup
            orientation="vertical"
            value={view}
            sx={{ width: "100%" }}
          >
            {payment_method.map((pm, index) => {
              return (
                <ToggleButton
                  key={index}
                  alignItems="start"
                  value={pm.payment_id}
                  onClick={() => handleChange(pm.payment_id)}
                  fullWidth={true}
                  sx={{
                    width: "100%",
                    gap: "10px",
                  }}
                >
                  <Box>
                    <img
                      src={
                        process.env.REACT_APP_BASE_URL_BE + "/" + pm.payment_pic
                      }
                      width={52}
                      alt={pm.payment_name}
                    />
                  </Box>
                  <Typography sx={{ flexGrow: 1, textAlign: "left" }}>
                    {pm.payment_name}
                  </Typography>
                </ToggleButton>
              );
            })}
          </ToggleButtonGroup>
          <Box
            justify="center"
            sx={{
              flexGrow: 1,
              marginTop: "24px",
              // border:1,borderColor:"black",
            }}
          >
            <Button
              variant="outlined"
              color="primary"
              sx={{
                borderRadius: "8px",
                width: {
                  xs: "70px",
                  sm: "70px",
                  md: "100px",
                  lg: "150px",
                  xl: "150px",
                },
                paddingX: "20px",
                paddingY: "10px",
                marginRight: "10px",
              }}
              onClick={handleClose}
            >
              Cancel
            </Button>
            <Button
              variant="contained"
              color="primary"
              sx={{
                borderRadius: "8px",
                width: {
                  xs: "70px",
                  sm: "70px",
                  md: "100px",
                  lg: "150px",
                  xl: "150px",
                },
                paddingX: "20px",
                paddingY: "10px",
              }}
              onClick={() => proceedCheckout()}
            >
              Pay Now
            </Button>
          </Box>
        </Box>
      </Modal>
    </>
  );
}
