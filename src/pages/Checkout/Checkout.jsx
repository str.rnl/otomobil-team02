import { Box, Grid, Typography, Checkbox, Button } from "@mui/material";
import React, { useState, useEffect } from "react";
import TrashCan from "../../assets/trash.png";
import FooterSum from "./FooterSum/FooterSum";
import axios from "../../utils/axiosConfig";
import formattedRupiah from "../../utils/formattedRupiah";
import formattedDate from "../../utils/formattedDate";

export default function Checkout() {
  const [carts, setCarts] = useState([]);
  const [payments, setPayments] = useState([]);

  const userFromLocalStorage = localStorage.getItem("user");
  const dataUser = JSON.parse(userFromLocalStorage);
  let user_id = 0;
  if (userFromLocalStorage) user_id = dataUser.users_id;

  useEffect(() => {
    const fetchCarts = async () => {
      try {
        const { data } = await axios.get("/api/cart/get_cart", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
          params: {
            uId: user_id,
          },
        });

        if (data) {
          const datas = data.map((obj) => ({ ...obj, isChecked: false }));
          setCarts(datas);
        }
      } catch (error) {
        console.error("failed fetching data carts", error);
      }
    };

    const fetchPaymentMethods = async () => {
      try {
        const { data } = await axios.get("/payment_active", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
        });

        if (data) {
          setPayments(data);
        }
      } catch (error) {
        console.error("failed fetching data carts", error);
      }
    };

    fetchCarts();
    fetchPaymentMethods();
    //setCartList(filt);
  }, []);

  const handleDeleteById = async (id) => {
    try {
      const { status } = await axios.delete("/api/cart/delete/" + id, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
        },
      });

      if (status === 200) {
        setCarts((oldValues) => {
          return oldValues.filter((item) => item.cart_id !== id);
        });
      }
    } catch (error) {
      console.error("failed deleting data carts", error);
    }
  };
  // const handleChecking = (e) =>{
  //   const {name, checked} = e.target;
  //   console.log(`item ${name} check status: ${checked}`)
  // }
  const handleChange = (e, id) => {
    const { name, checked } = e.target;
    if (name === "allSelect" && id === 0) {
      let allList = carts.map((item) => {
        return { ...item, isChecked: checked };
      });
      setCarts(allList);
    } else {
      let tempList = carts.map((item) =>
        item.cart_id === id ? { ...item, isChecked: checked } : item
      );
      //console.log(`item ${name} check status: ${!checked}`);
      console.log(checked);
      setCarts(tempList);
      //setPayList(tempList);
    }
  };



  return (
    <>
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        sx={{
          // border: 1,
          // borderColor:"black",
          width: "100%",
          minHeight: "500px",
        }}
      >
        <Box
          sx={{
            display: "flex",
            borderBottom: 1,
            borderColor: "black",
            width: "80%",
            minHeight: "10px",
            marginTop: "10px",
            marginX: "40px",
            paddingBottom: "10px",
          }}
        >
          <Checkbox
            // defaultChecked
            // disableRipple
            color="primary"
            id="allSelect"
            name="allSelect"
            onChange={(e) => handleChange(e, 0)}
            checked={
              carts.filter((item) => item?.isChecked !== true).length < 1
            }
            sx={{
              marginRight: "10px",
              // border: 1,
              // borderColor:"primary",
            }}
          />
          <Typography
            sx={{
              fontWeight: 300,
              fontSize: "20px",
            }}
          >
            Pilih Semua
          </Typography>
        </Box>
        {carts &&
          carts.map((item, index) => {
            return (
              <Box
                key={index}
                justifyContent="space-between"
                alignItems="center"
                sx={{
                  display: "flex",
                  borderBottom: 1,
                  // border: 1,
                  borderColor: "black",
                  width: "80%",
                  minHeight: "20px",
                  marginTop: "20px",
                  marginX: "50px",
                  paddingBottom: "20px",
                  paddingRight: "10px",
                }}
              >
                <Checkbox
                  // disableRipple
                  color="primary"
                  id={item.card_id}
                  name={item.products.product_name}
                  checked={item.isChecked}
                  //checked={item?.isChecked || false}
                  onChange={(e) => handleChange(e, item.cart_id)}
                  sx={{
                    marginRight: "10px",
                    // border: 1,
                    // borderColor:"primary",
                  }}
                />
                <Grid
                  container
                  spacing={0}
                  sx={{
                    display: "flex",
                    // border: 1,
                    // borderColor:"blue",
                  }}
                >
                  <img
                    src={`${process.env.REACT_APP_BASE_URL_BE}/${item.products.product_pic}`}
                    alt={item.products.product_description}
                    width="200px"
                    height="135px"
                    sx={
                      {
                        // border: 2,
                        // borderColor:"blue",
                      }
                    }
                  ></img>
                  <Box
                    justifyItems="space-evenly"
                    sx={{
                      // border: 1,
                      // borderColor:"yellow",
                      marginLeft: "20px",
                      paddingTop: "5px",
                    }}
                  >
                    <Typography
                      gutterBottom
                      variant="span"
                      color="text.secondary"
                      component="div"
                      marginBottom="10px"
                    >
                      {item.products.category.category_name}
                    </Typography>
                    <Typography
                      variant="h6"
                      color="text.primary"
                      marginBottom="10px"
                      sx={{ fontWeight: 600 }}
                    >
                      {item.products.product_name}
                    </Typography>
                    <Typography
                      gutterBottom
                      variant="span"
                      color="text.secondary"
                      component="div"
                      marginBottom="10px"
                    >
                      Schedule: {formattedDate(item.date)}
                    </Typography>
                    <Typography
                      variant="h6"
                      color="primary"
                      sx={{ fontWeight: 600 }}
                    >
                      IDR {formattedRupiah(item.products.product_price)}
                    </Typography>
                  </Box>
                </Grid>

                <Button onClick={() => handleDeleteById(item.cart_id)}>
                  <img
                    src={TrashCan}
                    alt="trash"
                    width="20px"
                    height="25px"
                    sx={{
                      // border: 2,
                      // borderColor:"blue",
                      component: "button",
                    }}
                  ></img>
                </Button>
              </Box>
            );
          })}
      </Grid>
      <FooterSum
        user_id={user_id}
        data_based_id={carts.filter((item) => item.isChecked)}
        payment_method={payments}
      />
    </>
  );
}