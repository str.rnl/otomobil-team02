import { Box, Button, Typography } from "@mui/material";
import SuccessIllustration from "../../assets/success_illustration.svg";
import { useNavigate } from "react-router-dom";

function Confirmation() {
  const navigate = useNavigate();
  return (
    <>
      <Box
        width="fullWidth"
        sx={{
          position: "relative",
        }}
      >
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, 50%)",
            textAlign: "center",
            width: "90%",
          }}
        >
          <Box>
            <img src={SuccessIllustration} alt="icon-success" />
          </Box>
          <Box>
            <Typography
              variant="h5"
              color="primary"
              sx={{ fontWeight: "500", marginBottom: "8px" }}
            >
              Email Confirmation Success
            </Typography>
            <Typography
              component="p"
              color="text.secondary"
              sx={{ marginBottom: "40px" }}
            >
              Your email already! Please login first to access the web
            </Typography>
            <Button
              onClick={() => navigate("/login")}
              variant="contained"
              color="primary"
              sx={{ padding: "16px 24px" }}
            >
              <Typography component="span" sx={{ fontWeight: 600 }}>
                Login Here
              </Typography>
            </Button>
          </Box>
        </Box>
      </Box>
    </>
  );
}

export default Confirmation;
