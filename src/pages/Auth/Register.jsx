import { Container, Box, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useCheckValue } from "../../hooks/useCheckValue";
import axios from "../../utils/axiosConfig";
import MyAlert from "../../components/MyAlert/MyAlert";
import { LoadingButton } from "@mui/lab";
import { useAuth } from "../../contexts/AuthContext";

function Register() {
  const [formData, setFormData] = useState({
    name: "",
    email: "",
    password: "",
    confirm_password: "",
  });

  const [validationErrors, setValidationErrors] = useState({
    name: "",
    email: "",
    password: "",
    confirm_password: "",
  });

  const [isAlert, setIsAlert] = useState(false);
  const [alertMsg, setAlertMsg] = useState({
    type: "",
    msg: "",
  });

  const [isSubmit, setIsSubmit] = useState(false);

  const checkForm = useCheckValue(formData);
  const checkValidationErrors = useCheckValue(validationErrors);

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
    validateInput(name, value);
  };

  const validateInput = (name, value) => {
    const errors = {};
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (value.length === 0) {
      errors[name] = `The field ${name} is required`;
    } else if (name === "email" && !emailRegex.test(value)) {
      errors[name] = "The email address is not valid";
    } else if (
      (name === "password" && value.length < 5) ||
      (name === "confirm_password" && value.length < 5)
    ) {
      errors[name] = "The field password must minimum length of 5";
    } else if (name === "confirm_password" && value !== formData.password) {
      errors[name] = "The field confirm_password not same with field password";
    } else {
      errors[name] = "";
    }

    setValidationErrors({ ...validationErrors, ...errors });
  };

  const handleSubmitForm = async (e) => {
    e.preventDefault();

    if (checkValidationErrors && !checkForm) {
      try {
        setIsSubmit(true);
        const response = await axios.post("/api/register", {
          users_name: formData.name,
          users_email: formData.email.toLowerCase(),
          users_password: formData.password,
        });

        if (response.status === 200) {
          setIsAlert(true);
          setAlertMsg({
            ...alertMsg,
            type: "Success",
            msg: response.data.message,
          });
          setIsSubmit(false);
        }
      } catch (error) {
        setIsAlert(true);
        setAlertMsg({
          ...alertMsg,
          type: "Error",
          msg: JSON.stringify(error.response.data),
        });
        setIsSubmit(false);
      }
    }
  };

  return (
    <>
      <Container fixed>
        <Box sx={{ marginTop: "96px", marginBottom: "40px" }}>
          <Typography
            variant="h4"
            color="primary"
            sx={{ marginBottom: "16px", fontWeight: 500 }}
          >
            Let's Join our course!
          </Typography>
          <Typography variant="h6" sx={{ color: "#4F4F4F" }}>
            Please register first!
          </Typography>
        </Box>

        <MyAlert
          isAlert={isAlert}
          setIsAlert={setIsAlert}
          alertMsg={alertMsg}
        />

        <Box>
          <form onSubmit={handleSubmitForm}>
            <Box sx={{ display: "flex", flexDirection: "column", gap: "32px" }}>
              <TextField
                autoFocus
                type="text"
                fullWidth
                label="Name"
                variant="outlined"
                size="small"
                value={formData.name}
                onChange={handleChangeInput}
                onBlur={handleChangeInput}
                name="name"
                error={validationErrors.name ? true : false}
                helperText={
                  validationErrors.name === "" ? "" : validationErrors.name
                }
                disabled={isSubmit}
              />
              <TextField
                type="email"
                fullWidth
                label="Email"
                variant="outlined"
                size="small"
                value={formData.email}
                onChange={handleChangeInput}
                onBlur={handleChangeInput}
                name="email"
                error={validationErrors.email ? true : false}
                helperText={
                  validationErrors.email === "" ? "" : validationErrors.email
                }
                disabled={isSubmit}
              />
              <TextField
                type="password"
                fullWidth
                label="Password"
                variant="outlined"
                size="small"
                value={formData.password}
                onChange={handleChangeInput}
                onBlur={handleChangeInput}
                name="password"
                error={validationErrors.password ? true : false}
                helperText={
                  validationErrors.password === ""
                    ? ""
                    : validationErrors.password
                }
                disabled={isSubmit}
              />
              <TextField
                type="password"
                fullWidth
                label="Confirm Password"
                variant="outlined"
                size="small"
                value={formData.confirm_password}
                onChange={handleChangeInput}
                onBlur={handleChangeInput}
                name="confirm_password"
                error={validationErrors.confirm_password ? true : false}
                helperText={
                  validationErrors.confirm_password === ""
                    ? ""
                    : validationErrors.confirm_password
                }
                disabled={isSubmit}
              />
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "end",
                gap: "34px",
                marginTop: "50px",
              }}
            >
              <LoadingButton
                type="submit"
                variant="contained"
                loading={isSubmit}
                color="primary"
                sx={{ width: "120px", borderRadius: "8px" }}
              >
                Sign Up
              </LoadingButton>
            </Box>
            <Box sx={{ textAlign: "center", marginTop: "60px" }}>
              <Typography component="p">
                Have acount?{" "}
                <Link style={{ textDecoration: "none" }} to="/login">
                  Login here
                </Link>
              </Typography>
            </Box>
          </form>
        </Box>
      </Container>
    </>
  );
}

export default Register;
