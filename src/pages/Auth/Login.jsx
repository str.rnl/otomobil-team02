import { Container, Box, TextField, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useCheckValue } from "../../hooks/useCheckValue";
import axios from "../../utils/axiosConfig";
import { LoadingButton } from "@mui/lab";
import MyAlert from "../../components/MyAlert/MyAlert";
import { useAuth } from "../../contexts/AuthContext";

function Login() {
  const [formData, setFormData] = useState({
    email: "",
    password: "",
  });

  const { setIsAuth } = useAuth();

  const [validationErrors, setValidationErrors] = useState({
    email: "",
    password: "",
  });

  const [isAlert, setIsAlert] = useState(false);
  const [alertMsg, setAlertMsg] = useState({
    type: "",
    msg: "",
  });

  const [isSubmit, setIsSubmit] = useState(false);

  const checkFormData = useCheckValue(formData);
  const checkValidationErrors = useCheckValue(validationErrors);

  const navigate = useNavigate();

  const handleChangeInput = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
    validateInput(name, value);
  };

  const validateInput = (name, value) => {
    const errors = {};
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/; //email regex
    if (value.length === 0) {
      errors[name] = `The field ${name} is required`;
    } else if (name === "email" && !emailRegex.test(value)) {
      errors[name] = "The email address is not valid";
    } else if (name === "password" && value.length < 5) {
      errors[name] = "The field password must minimum length of 5";
    } else {
      errors[name] = "";
    }

    setValidationErrors({ ...validationErrors, ...errors });
  };

  const handleLogin = async (e) => {
    e.preventDefault();

    if (checkValidationErrors && !checkFormData) {
      try {
        setIsSubmit(true);
        const response = await axios.post("/api/login", {
          email: formData.email,
          password: formData.password,
        });

        console.log(response);

        const { data, status } = response;

        if (status === 200) {
          setIsSubmit(false);
          const { access_token, user } = data;

          localStorage.setItem("access_token", access_token);
          localStorage.setItem("user", JSON.stringify(user));
          setIsAuth(true);

          navigate("/");
        }
      } catch (error) {
        setIsAlert(true);
        setAlertMsg({
          ...alertMsg,
          type: "Error",
          msg: error.response.data.message,
        });
        setIsSubmit(false);
      }
    }
  };

  return (
    <>
      <Container fixed>
        <Box sx={{ marginTop: "96px", marginBottom: "40px" }}>
          <Typography
            variant="h4"
            color="primary"
            sx={{ marginBottom: "16px", fontWeight: 500 }}
          >
            Welcome Back!
          </Typography>
          <Typography variant="h6" sx={{ color: "#4F4F4F" }}>
            Please login first!
          </Typography>
        </Box>

        <MyAlert
          isAlert={isAlert}
          setIsAlert={setIsAlert}
          alertMsg={alertMsg}
        />

        <Box>
          <form onSubmit={handleLogin}>
            <Box sx={{ display: "flex", flexDirection: "column", gap: "32px" }}>
              <TextField
                autoFocus
                type="email"
                fullWidth
                label="Email"
                variant="outlined"
                size="small"
                value={formData.email}
                onChange={handleChangeInput}
                onBlur={handleChangeInput}
                name="email"
                error={validationErrors.email ? true : false}
                helperText={
                  validationErrors.email === "" ? "" : validationErrors.email
                }
                disabled={isSubmit}
                required
              />
              <TextField
                type="password"
                fullWidth
                label="Password"
                variant="outlined"
                size="small"
                value={formData.password}
                onChange={handleChangeInput}
                onBlur={handleChangeInput}
                name="password"
                error={validationErrors.password ? true : false}
                helperText={
                  validationErrors.password === ""
                    ? ""
                    : validationErrors.password
                }
                disabled={isSubmit}
                required
              />
              <Typography component="p">
                Forget Password?{" "}
                <Link
                  style={{ textDecoration: "none" }}
                  to={`/reset-password?type=confirm_email`}
                >
                  Click Here!
                </Link>
              </Typography>
            </Box>
            <Box
              sx={{
                display: "flex",
                justifyContent: "end",
                gap: "34px",
                marginTop: "50px",
              }}
            >
              <LoadingButton
                type="submit"
                variant="contained"
                color="primary"
                loading={isSubmit}
                sx={{ width: "120px", borderRadius: "8px" }}
              >
                Login
              </LoadingButton>
            </Box>
            <Box sx={{ textAlign: "center", marginTop: "60px" }}>
              <Typography component="p">
                Don't have acount?{" "}
                <Link style={{ textDecoration: "none" }} to="/register">
                  Sign Up here
                </Link>
              </Typography>
            </Box>
          </form>
        </Box>
      </Container>
    </>
  );
}

export default Login;
