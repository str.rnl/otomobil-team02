import { Box, Button, Typography } from "@mui/material";
import SuccessIllustration from "../../assets/success_illustration.svg";
import { useNavigate, useSearchParams } from "react-router-dom";
import { useEffect, useState } from "react";
import axios from "../../utils/axiosConfig";

function SuccessConfirm() {
  const navigate = useNavigate();
  const [params] = useSearchParams();
  const [error, setError] = useState("");
  const user_id = decodeURIComponent(params.get("q"));

  const sendConfirmationEmail = async (user_id) => {
    const payload = {
      users_id: user_id,
    };

    try {
      const response = await axios.post(
        "/api/reset-password/create-form",
        payload
      );

      return response.data;
    } catch (error) {
      throw error;
    }
  };

  useEffect(() => {
    sendConfirmationEmail(user_id)
      .then(() => {
        setError("");
      })
      .catch((error) => {
        setError("Failed to Create Form Reset Password, error: " + error);
      });
  }, [user_id]);
  return (
    <>
      <Box
        width="fullWidth"
        sx={{
          position: "relative",
        }}
      >
        <Box
          sx={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, 50%)",
            textAlign: "center",
            width: "90%",
          }}
        >
          <Box>
            <img src={SuccessIllustration} alt="icon-success" />
          </Box>
          <Box>
            <Typography
              variant="h5"
              color="primary"
              sx={{ fontWeight: "500", marginBottom: "8px" }}
            >
              Email Confirmation Success
            </Typography>
            <Typography
              component="p"
              color="text.secondary"
              sx={{ marginBottom: "40px" }}
            >
              Please check your email address! We've already send a link to
              reset your password
            </Typography>
            <Button
              onClick={() => navigate("/")}
              variant="contained"
              color="primary"
              sx={{ padding: "16px 24px" }}
            >
              <Typography component="span" sx={{ fontWeight: 600 }}>
                Back to Home
              </Typography>
            </Button>
          </Box>
        </Box>
      </Box>
    </>
  );
}

export default SuccessConfirm;
