import { Container } from "@mui/material";
import React, { useEffect } from "react";
import FormResetPassword from "../../components/ResetPassword/FormResetPassword";
import HeaderResetPassword from "../../components/ResetPassword/HeaderResetPassword";
import { useNavigate, useSearchParams } from "react-router-dom";

export default function ResetPassword() {
  const [searchParams] = useSearchParams();
  const type = searchParams.get("type");
  const q = searchParams.get("q");
  const navigate = useNavigate();
  useEffect(() => {
    if (type == null || q == null) {
      navigate("/reset-password?type=confirm_email");
    }
  }, [navigate, type, q]);

  return (
    <Container fixed>
      <HeaderResetPassword type={type} />
      <FormResetPassword type={type} />
    </Container>
  );
}
