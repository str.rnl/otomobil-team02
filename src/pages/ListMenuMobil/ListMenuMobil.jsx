import { Box, Container, Typography } from "@mui/material";
import React, { useEffect } from "react";

import BgMobil from "../../assets/bg-mobil.jpg";
import ItemContainer from "../../components/Item/ItemContainer";
import { useItem } from "../../contexts/ItemContext";
import { useSearchParams } from "react-router-dom";

const ListMenuMobil = () => {
  const [searchParams] = useSearchParams();
  const cat_id = searchParams.get("cat_id");

  const { category, fetchListItem } = useItem();

  useEffect(() => {
    fetchListItem(cat_id);
  }, [cat_id]);

  return (
    <>
      <Box
        sx={{
          width: "100%",
          height: { xs: "200px", md: "350px" },
          backgroundImage: `url(${BgMobil})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
          backgroundPosition: "center",
          marginBottom: "46px",
        }}
      ></Box>
      <Container maxWidth="xl" sx={{ marginBottom: "80px" }}>
        <Typography
          variant="h5"
          color="text.primary"
          sx={{ marginBottom: "16px" }}
        >
          {category.category_name}
        </Typography>
        <Typography component="p" color="text.primary">
          {category.category_description}
        </Typography>
      </Container>
      <Box height="1px" sx={{ background: "#E0E0E0", border: "none" }}></Box>;
      <Container
        maxWidth="xl"
        sx={{ marginTop: "80px", marginBottom: "100px" }}
      >
        <ItemContainer title="Another favourite course" />
      </Container>
    </>
  );
};

export default ListMenuMobil;
