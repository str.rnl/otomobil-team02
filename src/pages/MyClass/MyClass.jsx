import { Box, Grid, Typography } from "@mui/material";
import React, { useEffect, useState } from "react";
import MobilImage from "../../assets/mobil2.jpg";
import MobilImage2 from "../../assets/mobil4.jpg";
import MobilImage3 from "../../assets/mobil5.jpg";
import MobilImage4 from "../../assets/mobil6.jpg";
import MobilImage5 from "../../assets/mobil7.jpg";
import MobilImage6 from "../../assets/mobil8.jpg";
import axios from "../../utils/axiosConfig";
import formattedDate from "../../utils/formattedDate";

export default function MyClass() {
  const dataMobil = [
    // {
    //   product_id: 1,
    //   Product_name: "Kijang Innova",
    //   product_price: 700000,
    //   product_description:"old but gold"
    //   product_pic: MobilImage,
    //   fk_category: 4
    // },
    // sementara pakai yang di bawah ini
    {
      id: 1,
      name: "Kijang Innova",
      category: "SUV",
      price: "700.000",
      desc: "kijang inova putih bersih, cocok untuk pemula",
      pic: MobilImage,
    },
    {
      id: 2,
      name: "Honda Brio",
      category: "LCGC",
      price: "500.000",
      desc: "mobil kecil gesit bisa masuk gang sempit",
      pic: MobilImage2,
    },
    {
      id: 3,
      name: "Hyundai Palisade 2021",
      category: "SUV",
      price: "800.000",
      desc: "SUV mewah berwarna coklat bawaan dari jepang",
      pic: MobilImage3,
    },
    {
      id: 4,
      name: "Mitsubitshi Pajero",
      category: "SUV",
      price: "800.000",
      desc: "mobil elit tanjakan tidak sulit",
      pic: MobilImage4,
    },
    {
      id: 5,
      name: "Dump Truck for Mining Constructor",
      category: "Truck",
      price: "1.200.000",
      desc: "memiliki roda sebesar kolam renang",
      pic: MobilImage5,
    },
    {
      id: 6,
      name: "Sedan Honda Civic",
      category: "Sedan",
      price: "400.000",
      desc: "nyetir santai dan elegan pakai sedan ini",
      pic: MobilImage6,
    },
  ];
  const num = 4;
  const filt = dataMobil.filter(
    (mycourse) => mycourse.id === parseInt(num || 3)
  );

  const userFromLocalStorage = localStorage.getItem("user");
  const dataUser = JSON.parse(userFromLocalStorage);
  let user_id = 0;
  if (userFromLocalStorage) user_id = dataUser.users_id;

  const [myClasses, setMyClasses] = useState([]);

  useEffect(() => {
    const fetchClasses = async () => {
      try {
        const { data } = await axios.get("/get_myclass", {
          headers: {
            Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          },
          params: {
            uId: user_id,
          },
        });

        if (data) {
          setMyClasses(data);
        }
      } catch (error) {
        console.error("failed fetching data carts", error);
      }
    };

    fetchClasses();
  }, []);

  return (
    <>
      <Grid
        container
        spacing={0}
        direction="column"
        alignItems="center"
        sx={{
          // border: 1,
          // borderColor:"black",
          width: "100%",
          minHeight: "800px",
        }}
      >
        {myClasses.map((my) => {
          return (
            <Box
              justifyContent="space-between"
              alignItems="center"
              sx={{
                display: "flex",
                borderBottom: 1,
                // border: 1,
                borderColor: "black",
                width: "80%",
                minHeight: "20px",
                marginTop: "20px",
                marginX: "50px",
                paddingBottom: "20px",
              }}
            >
              <Grid
                container
                spacing={0}
                sx={{
                  display: "flex",
                  // border: 1,
                  // borderColor:"blue",
                }}
              >
                <img
                  src={
                    process.env.REACT_APP_BASE_URL_BE +
                    "/" +
                    my.products.product_pic
                  }
                  alt={my.products.prodcut_name}
                  width="200px"
                  height="135px"
                  sx={
                    {
                      // border: 2,
                      // borderColor:"blue",
                    }
                  }
                ></img>
                <Box
                  justifyItems="space-evenly"
                  sx={{
                    // border: 1,
                    // borderColor:"yellow",
                    marginLeft: "20px",
                  }}
                >
                  <Typography
                    gutterBottom
                    variant="span"
                    color="text.secondary"
                    component="div"
                    marginBottom="10px"
                    marginTop="15px"
                  >
                    {my.products.category.category_name}
                  </Typography>
                  <Typography
                    variant="h6"
                    color="text.primary"
                    marginBottom="10px"
                    sx={{ fontWeight: 600 }}
                  >
                    {my.products.product_name}
                  </Typography>
                  <Typography
                    variant="h6"
                    color="primary"
                    sx={{ fontWeight: 600 }}
                  >
                    Schedule: {formattedDate(my.date)}
                  </Typography>
                </Box>
              </Grid>
            </Box>
          );
        })}
      </Grid>
    </>
  );
}
